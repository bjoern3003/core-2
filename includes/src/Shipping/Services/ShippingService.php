<?php

declare(strict_types=1);

namespace JTL\Shipping\Services;

use JTL\Abstracts\AbstractService;
use JTL\Cart\Cart;
use JTL\Cart\CartItem;
use JTL\Catalog\Currency;
use JTL\Catalog\Product\Artikel;
use JTL\Catalog\Product\Preise;
use JTL\Checkout\Lieferadresse;
use JTL\Country\Country;
use JTL\Customer\Customer;
use JTL\Customer\CustomerGroup;
use JTL\Helpers\Request;
use JTL\Helpers\Tax;
use JTL\Language\LanguageHelper;
use JTL\Language\LanguageModel;
use JTL\Services\JTL\CountryServiceInterface;
use JTL\Settings\Option\Checkout;
use JTL\Settings\Option\Customer as CustomerOption;
use JTL\Settings\Settings;
use JTL\Shipping\DomainObjects\PaymentDTO;
use JTL\Shipping\DomainObjects\ShippingCartPositionDTO;
use JTL\Shipping\DomainObjects\ShippingDTO;
use JTL\Shipping\DomainObjects\ShippingSurchargeDTO;
use JTL\Shipping\Helper\ProductDependentShippingCosts;
use JTL\Shipping\Helper\ShippingCalculationMethod;
use JTL\Shipping\Repositories\CacheRepository;
use JTL\Shipping\Repositories\DatabaseRepository;
use JTL\Shipping\Repositories\SessionRepository;
use JTL\Shop;
use JTL\Traits\FloatingPointTrait;
use RuntimeException;

/**
 * Class ShippingService
 * @package JTL\Shipping
 * @since 5.5.0
 * @todo Implement $this->cart->gibGesamtsummeWarenExt() inside this service in order to be prepared for SHOP-4115
 */
class ShippingService extends AbstractService
{
    use FloatingPointTrait;

    public readonly string $deliveryCountryCode;

    public readonly string $deliveryZipCode;

    private readonly CustomerGroup $customerGroup;

    private ?LanguageHelper $languageHelper = null;

    /**
     * @var LanguageModel[]
     */
    private ?array $availableLangs = null;

    private ?CountryServiceInterface $countryService = null;

    /**
     * @var array<int, array<int, array<int, ShippingDTO>>>
     * [customerGroupID][shippingClassID] => ShippingDTO[]
     */
    private array $countriesWithFreeShipping = [];

    /**
     * @description Properties that are used to calculate the checksum of the cart, in order to detect changes that
     *  are relevant for shipping calculation. The keys are mapped directly with their values to the CartItem properties
     * @comment Is used inside the session repository in order to retrieve and store possible shipping methods:
     *  [countryISO + zipCode + checksum] => ShippingDTO[]
     */
    private const CART_CHECKSUM_SHIPPING_PROPERTIES = [
        'id'           => 'kArtikel',
        'qty'          => 'nAnzahl',
        'netUnitPrice' => 'fPreisEinzelNetto',
        'netPrice'     => 'fPreis',
        'weight'       => 'fGesamtgewicht',
    ];

    /**
     * @param array<string, mixed> $config
     */
    public function __construct(
        protected readonly Cart $cart,
        protected readonly Customer $customer,
        protected readonly Currency $currency,
        protected readonly array $config,
        protected readonly DatabaseRepository $database = new DatabaseRepository(),
        protected readonly CacheRepository $cache = new CacheRepository(),
        protected readonly SessionRepository $session = new SessionRepository(),
    ) {
        $customerID          = $this->customer->getID();
        $this->customerGroup = $this->getCustomerGroup();
        if ($this->session->hasEnoughDeliveryData() === false) {
            $deliveryAddress = $this->getDeliveryAddressFromUI();
            if ($deliveryAddress !== null) {
                $this->session->setDeliveryAddress($deliveryAddress);
            }
        }
        $deliveryAddress ??= $this->session->getDeliveryAddress();
        if (
            $deliveryAddress === null
            && $customerID > 0
        ) {
            $deliveryAddress = $this->database->getPreferredDeliveryAddress($customerID);
            if ($deliveryAddress !== null) {
                $this->session->setDeliveryAddress($deliveryAddress);
            }
            // Could use data from last order if no preferred delivery address is set.
        }

        $this->deliveryCountryCode = $deliveryAddress->cLand
            ?? $this->customer->cLand
            ?? Settings::stringValue(CustomerOption::DELIVERY_ADDRESS_COUNTRY_PROMPT);
        $this->deliveryZipCode     = $deliveryAddress->cPLZ
            ?? $this->customer->cPLZ
            ?? '';

        if ((string)($_SESSION['cLieferlandISO'] ?? '') !== $this->deliveryCountryCode) {
            Tax::setTaxRates($this->deliveryCountryCode);
        }
    }

    public function getLanguageHelper(): LanguageHelper
    {
        return $this->languageHelper ?? $this->setLanguageHelper();
    }

    public function setLanguageHelper(?LanguageHelper $languageHelper = null): LanguageHelper
    {
        $this->languageHelper ??= $languageHelper ?? Shop::Lang();
        return $this->languageHelper;
    }

    public function getCountryService(): CountryServiceInterface
    {
        return $this->countryService ?? $this->setCountryService();
    }

    public function setCountryService(?CountryServiceInterface $countryService = null): CountryServiceInterface
    {
        $this->countryService ??= $countryService ?? Shop::Container()->getCountryService();
        return $this->countryService;
    }

    /**
     * @return LanguageModel[]
     */
    private function getAvailableLanguages(): array
    {
        $this->availableLangs ??= $this->getLanguageHelper()->getAvailable();
        return $this->availableLangs;
    }

    public function getDeliveryAddressFromUI(): ?Lieferadresse
    {
        $deliveryAddress = null;
        if (Request::postVar('versandrechnerBTN') === null) {
            return null;
        }
        $deliveryCountry = Request::postVar('land');
        $deliveryZipCode = Request::postVar('plz');
        if (
            \is_string($deliveryCountry)
            && \is_string($deliveryZipCode)
            && $deliveryZipCode !== ''
            && \strlen($deliveryCountry) === 2
        ) {
            $deliveryAddress        = new Lieferadresse();
            $deliveryAddress->cLand = $deliveryCountry;
            $deliveryAddress->cPLZ  = $deliveryZipCode;
        }

        return $deliveryAddress;
    }

    /**
     * @return DatabaseRepository
     */
    protected function getRepository(): DatabaseRepository
    {
        return $this->database;
    }

    public function getCustomerGroup(): CustomerGroup
    {
        $customerGroup = $this->session->getCustomerGroup();
        if (
            $customerGroup !== null
            && $customerGroup->getID() === $this->customer->getGroupID()
        ) {
            return $customerGroup;
        }
        return new CustomerGroup($this->customer->getGroupID());
    }

    /**
     * @param CartItem[] $cartItems
     */
    private function getCartChecksum(array $cartItems, ?string $countryCode = null): string
    {
        $result               = [];
        $countryCode          ??= $this->deliveryCountryCode;
        $basketHasCustomCosts = false;
        foreach ($cartItems as $item) {
            foreach (self::CART_CHECKSUM_SHIPPING_PROPERTIES as $offset => $property) {
                if (isset($item->$property) === false) {
                    continue;
                }
                $result[$offset][] = $item->$property;
            }
            if ($basketHasCustomCosts === false) {
                $basketHasCustomCosts = $item->isUsedForShippingCostCalculation($countryCode) === false;
            }
        }
        $result['isDependent'] = $basketHasCustomCosts;

        return \md5(\serialize($result));
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getShippingMethods()
     * @return ShippingDTO[]
     */
    public function getAllShippingMethods(): array
    {
        return $this->database->getAllShippingMethods(
            $this->getAvailableLanguages()
        );
    }

    /**
     * @param ShippingDTO[] $shippingMethods
     * @todo Should we at least check for shipping country? The coupon could not be appliable.
     */
    private function applyShippingFreeCoupon(array &$shippingMethods): bool
    {
        if (!empty($this->session->get('VersandKupon'))) {
            $shippingMethods = \array_map(
                function (ShippingDTO $method): ShippingDTO {
                    return $method->setPrices(
                        (object)[
                            'finalNetCost'   => 0.00,
                            'finalGrossCost' => 0.00,
                            'localizedPrice' => Preise::getLocalizedPriceString(
                                0.00,
                                $this->currency
                            ),
                        ]
                    );
                },
                $shippingMethods
            );

            return true;
        }

        return false;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getPossibleShippingMethods()
     * @return ShippingDTO[]
     */
    public function getPossibleShippingMethods(
        string $deliveryCountryCode = '',
        string $deliveryZipCode = '',
    ): array {
        $deliveryCountryCode = $deliveryCountryCode ?: $this->deliveryCountryCode;
        $deliveryZipCode     = $deliveryZipCode ?: $this->deliveryZipCode;
        $customerGroupID     = $this->customerGroup->getID();
        $checksum            = $this->getCartChecksum($this->cart->PositionenArr, $deliveryCountryCode);
        $possibleMethods     = $this->session->getPossibleMethods(
            $deliveryCountryCode,
            $deliveryZipCode,
            $checksum,
        );
        if (\count($possibleMethods) === 0) {
            $possibleMethods = $this->getMethodsFromCacheOrDB(
                $deliveryCountryCode,
                $customerGroupID,
            );
            if (\count($possibleMethods) === 0) {
                return [];
            }

            $possibleMethods = $this->filterMethodsByCartItems(
                $possibleMethods,
                $deliveryCountryCode,
                $this->cart->PositionenArr,
            );
            $possibleMethods = $this->setOptionalDataInShippingMethods(
                $possibleMethods,
                $deliveryCountryCode,
                $deliveryZipCode,
            );
            $possibleMethods = $this->filterOutCheapestMethods($possibleMethods);
            $this->session->setPossibleMethods(
                $possibleMethods,
                $deliveryCountryCode,
                $deliveryZipCode,
                $checksum,
            );
        }
        $this->applyShippingFreeCoupon($possibleMethods);

        return $possibleMethods;
    }

    /**
     * @param ShippingDTO[] $shippingMethods
     * @return ShippingDTO[]
     */
    public function sortMethodsByPrice(array $shippingMethods): array
    {
        \usort(
            $shippingMethods,
            static function (ShippingDTO $methodA, ShippingDTO $methodB): int {
                return $methodA->finalNetCost <=> $methodB->finalNetCost;
            }
        );

        return $shippingMethods;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getDeliveryText()
     */
    public function getDeliveryText(
        int $minDeliveryDays,
        int $maxDeliveryDays,
        string $translationOffset
    ): string {
        if (\stripos($translationOffset, 'simple') === false) {
            return \str_replace(
                ['#MINDELIVERYTIME#', '#MAXDELIVERYTIME#'],
                [(string)$minDeliveryDays, (string)$maxDeliveryDays],
                $this->getLanguageHelper()->get($translationOffset)
            );
        }

        return \str_replace(
            '#DELIVERYTIME#',
            (string)$minDeliveryDays,
            $this->getLanguageHelper()->get($translationOffset)
        );
    }

    /**
     * @former JTL\Helpers\ShippingMethod\filter()
     * @return ShippingDTO[]
     */
    public function getPossibleFreeShippingMethods(
        string $country,
        string $zipCode,
        float $maxMinimumAmount = 999999999.99
    ): array {
        $freeShippingMethods = \array_filter(
            $this->getPossibleShippingMethods($country, $zipCode),
            function ($method) use ($maxMinimumAmount): bool {
                return $this->isZero($method->finalNetCost)
                    || (
                        $method->freeShippingMinAmount > 0
                        && $method->freeShippingMinAmount <= $maxMinimumAmount
                    );
            }
        );

        \usort(
            $freeShippingMethods,
            static fn(ShippingDTO $methodA, ShippingDTO $methodB) => $methodA->sort <=> $methodB->sort
        );

        return \array_values($freeShippingMethods);
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getFreeShippingCountries()
     * @param object{Preise: object{fVK: array<int, float>}, kVersandklasse: int} $product
     * @return string[]
     * @todo Make $product more explicit, setting the param to Artikel as soon as ShippingMethods.php gets removed.
     */
    public function getFreeShippingCountriesByProduct(
        object $product,
    ): array {
        $netPrice        = $product->Preise->fVK[1] ?? 0.00;
        $grossPrice      = $product->Preise->fVK[0] ?? 0.00;
        $shippingClassID = $product->kVersandklasse ?? 0;
        $customerGroupID = $this->customerGroup->getID();
        if (!isset($this->countriesWithFreeShipping[$customerGroupID][$shippingClassID])) {
            if (!isset($this->countriesWithFreeShipping[$customerGroupID])) {
                $this->countriesWithFreeShipping[$customerGroupID] = [];
            }
            $this->countriesWithFreeShipping[$customerGroupID][$shippingClassID] = $this->database
                ->getFreeShippingMethods(
                    $customerGroupID,
                    $shippingClassID
                );
        }
        $shippingFreeCountries = [];
        foreach ($this->countriesWithFreeShipping[$customerGroupID][$shippingClassID] as $shippingMethod) {
            if (
                $shippingMethod->freeShippingMinAmount >= (
                    $shippingMethod->includeTaxes
                    ? $grossPrice
                    : $netPrice
                )
            ) {
                continue;
            }
            foreach ($shippingMethod->countries as $country) {
                if (\in_array($country, $shippingFreeCountries, true)) {
                    continue;
                }
                $shippingFreeCountries[] = $country;
            }
        }

        return $shippingFreeCountries;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\normalerArtikelversand()
     * @param CartItem[] $cartItems
     * @description Returns true when $cartItems are only products with custom shipping costs for this country.
     */
    public function cartIsDependent(string $country = '', ?array $cartItems = null): bool
    {
        $result       = true;
        $productCount = 0;
        foreach ($cartItems ?? $this->cart->PositionenArr as $cartProduct) {
            if ((int)$cartProduct->nPosTyp !== \C_WARENKORBPOS_TYP_ARTIKEL || $cartProduct->Artikel === null) {
                continue;
            }
            $productCount++;
            if (
                $cartProduct->Artikel->isUsedForShippingCostCalculation(
                    $country ?: $this->deliveryCountryCode
                )
            ) {
                $result = false;
            }
        }

        return $productCount > 0 && $result;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\hasSpecificShippingcosts()
     * @param CartItem[] $cartItems
     * @description Returns true when there is at least one product with custom shipping costs inside the cart.
     */
    public function cartIsMixed(string $country = '', ?array $cartItems = null): bool
    {
        $result = false;
        foreach ($cartItems ?? $this->cart->PositionenArr as $cartProduct) {
            if (
                (int)$cartProduct->nPosTyp !== \C_WARENKORBPOS_TYP_ARTIKEL
                || $cartProduct->Artikel === null
            ) {
                continue;
            }
            if (
                $cartProduct->Artikel->isUsedForShippingCostCalculation(
                    $country ?: $this->deliveryCountryCode
                ) === false
            ) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param PaymentDTO[] $paymentMethods
     */
    public function filterPaymentMethodByID(array $paymentMethods, int $paymentMethodID): ?PaymentDTO
    {
        $filteredMethods = \array_values(
            \array_filter(
                $paymentMethods,
                static function (PaymentDTO $paymentMethod) use ($paymentMethodID): bool {
                    return $paymentMethod->id === $paymentMethodID;
                }
            )
        );

        return $filteredMethods[0] ?? null;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getPaymentMethods()
     * @return PaymentDTO[]
     * @todo Cache entry must be unset whenever associated shipping methods got modified or payment methods got deleted
     */
    public function getPaymentMethodsByShipping(int $shippingMethodID): array
    {
        $customerGroupID = $this->customerGroup->getID();
        $allowdMethods   = $this->cache->getPaymentMethods(
            $shippingMethodID,
            $customerGroupID
        );
        if ($allowdMethods !== []) {
            return $allowdMethods;
        }
        $allowdMethods = $this->database->getPaymentMethods(
            $shippingMethodID,
            $customerGroupID,
        );
        if ($allowdMethods !== []) {
            $this->cache->setPaymentMethods(
                $shippingMethodID,
                $customerGroupID,
                $allowdMethods
            );
        }

        return $allowdMethods;
    }

    public function getVatNote(): string
    {
        return $this->customerGroup->isMerchant()
            ? ' '
                . $this->getLanguageHelper()->get('plus', 'productDetails')
                . ' '
                . $this->getLanguageHelper()->get('vat', 'productDetails')
            : '';
    }

    /**
     * @param ShippingDTO[] $shippingMethods
     * @return ShippingDTO[]
     */
    private function setOptionalDataInShippingMethods(
        array $shippingMethods,
        string $country,
        string $zipCode,
    ): array {
        $result = [];
        foreach ($shippingMethods as $method) {
            $shippingSurcharge = $this->getShippingSurcharge(
                $method,
                $country,
                $zipCode,
            );
            if ($shippingSurcharge !== null) {
                $method = $method->setShippingSurcharge($shippingSurcharge);
            }

            $shippingNetFees = $this->calcCostForShippingMethod(
                $method,
                $country,
            );
            if ($shippingNetFees === -1.00) {
                continue;
            }

            $localizedPrice = $this->customerGroup->isMerchant()
                ? Preise::getLocalizedPriceString(
                    $shippingNetFees,
                    $this->currency,
                )
                : Preise::getLocalizedPriceString(
                    $this->calculateGrossPrice($shippingNetFees),
                    $this->currency
                );

            $result[] = $method
                ->setPrices(
                    (object)[
                        'finalNetCost'   => $shippingNetFees,
                        'finalGrossCost' => $this->calculateGrossPrice($shippingNetFees),
                        'localizedPrice' => match ($this->isZero($shippingNetFees)) {
                            true  => $this->getLanguageHelper()->get('freeshipping'),
                            false => $localizedPrice . $this->getVatNote(),
                        },
                    ]
                )->setCustomShippingCosts(
                    $this->getCustomShippingCostsByCart($country)
                );
        }

        return $result;
    }

    /**
     * @param ShippingDTO[] $shippingMethods
     * @return ShippingDTO[]
     */
    public function filterOutCheapestMethods(array $shippingMethods): array
    {
        $result = [];
        if (empty($shippingMethods)) {
            return $result;
        }

        $cheapestCost = \min(
            \array_map(
                static function (ShippingDTO $method): float {
                    return $method->exclFromCheapestCalc
                        ? 99999999.00
                        : $method->finalNetCost;
                },
                $shippingMethods
            )
        );
        foreach ($shippingMethods as $method) {
            if (
                $method->showAlways
                || $method->finalNetCost <= $cheapestCost
            ) {
                $result[] = $method;
            }
        }

        return $result;
    }

    /**
     * @former JTL\Catalog\Product\Artikel\getFavourableShipping()
     * @comment Replacing cache with session could give +- 120ms performance boost in some cases.
     */
    public function getFavourableShippingForProduct(
        Artikel $product,
        string $country,
    ): ?ShippingDTO {
        $cartItem        = $this->cartItemFromProduct($product);
        $possibleMethods = $this->getMethodsFromCacheOrDB(
            $country,
            $this->customerGroup->getID(),
        );
        if (\count($possibleMethods) === 0) {
            return null;
        }

        $itemTotalValue  = (float)$cartItem->nAnzahl * (float)$cartItem->fPreis;
        $possibleMethods = $this->filterMethodsByCartItems(
            $possibleMethods,
            $country,
            [$cartItem],
        );

        $finalCandidates = $this->setShippingMethodPrices(
            $possibleMethods,
            $itemTotalValue,
            $country,
            $product,
            $cartItem
        );

        $finalCandidates = $this->filterOutCheapestMethods($finalCandidates);
        if (empty($finalCandidates)) {
            return null;
        }

        return $this->sortMethodsByPrice($finalCandidates)[0];
    }

    /**
     * @former JTL\Helpers\ShippingMethod\gibHinzukommendeArtikelAbhaengigeVersandkosten()
     * @param ShippingDTO[] $dependentShippingMethods
     */
    public function simulateCustomShippingCosts(
        string $iso,
        array $dependentShippingMethods,
        Artikel $product,
        float $productAmount
    ): ?ShippingCartPositionDTO {
        $iso = $iso ?: $this->deliveryCountryCode;
        if (
            $product->kArtikel === null
            || $product->FunktionsAttribute === null
            || $iso === ''
        ) {
            return null;
        }

        if (\str_contains($product->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN_GESTAFFELT] ?? '', $iso)) {
            $productAmount += \array_sum(
                \array_map(
                    static fn($position) => (float)$position->nAnzahl,
                    \array_filter(
                        $this->cart->PositionenArr,
                        static fn($position) => (int)$position->kArtikel === $product->kArtikel
                    )
                )
            );
        }

        $dependentMethod = \array_reduce(
            $dependentShippingMethods,
            static function (?ShippingDTO $carry, ShippingDTO $method): ?ShippingDTO {
                return $method->dependentShipping ? $method : $carry;
            },
        );
        if ($dependentMethod instanceof ShippingDTO === false) {
            return null;
        }

        return $this->getCustomShippingCostsByProduct(
            $iso,
            $product,
            $productAmount,
            $dependentMethod
        );
    }

    /**
     * @former JTL\Helpers\ShippingMethod\pruefeArtikelabhaengigeVersandkosten()
     * @comment Cant take care of possible forgotten FunktionsAttribute. Only one value should be set at a time.
     */
    public function getCustomShippingCostType(Artikel $product): ProductDependentShippingCosts
    {
        $hookReturn = false;
        \executeHook(\HOOK_TOOLS_GLOBAL_PRUEFEARTIKELABHAENGIGEVERSANDKOSTEN, [
            'oArtikel'    => &$product,
            'bHookReturn' => &$hookReturn
        ]);

        if (
            $hookReturn
            || $product->FunktionsAttribute === null
        ) {
            return ProductDependentShippingCosts::NONE;
        }
        return match (false) {
            empty($product->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN])            =>
            ProductDependentShippingCosts::FIX,
            empty($product->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN_GESTAFFELT]) =>
            ProductDependentShippingCosts::BULK,
            default                                                                     =>
            ProductDependentShippingCosts::NONE,
        };
    }

    /**
     * @former JTL\Helpers\ShippingMethod\gibArtikelabhaengigeVersandkosten()
     */
    public function getCustomShippingCostsByProduct(
        string $country,
        Artikel $product,
        float $productQty,
        ShippingDTO $dependentShippingMethod,
    ): ?ShippingCartPositionDTO {
        $customCostType = $this->getCustomShippingCostType($product);
        if (
            $customCostType === ProductDependentShippingCosts::NONE
            || $dependentShippingMethod->dependentShipping === false
        ) {
            return null;
        }

        return $this->getCustomShippingCosts(
            $country ?: $this->deliveryCountryCode,
            $product,
            $productQty,
            $dependentShippingMethod,
        );
    }

    /**
     * @former JTL\Helpers\ShippingMethod\gibArtikelabhaengigeVersandkostenImWK()
     * @param CartItem[]|null $cartItems
     * @return ShippingCartPositionDTO[]
     */
    public function getCustomShippingCostsByCart(string $country, ?array $cartItems = null): array
    {
        $result  = [];
        $country = $country ?: $this->deliveryCountryCode;

        $cartItemsWithCustomCosts = \array_filter(
            $cartItems ?? $this->cart->PositionenArr,
            function (CartItem $item) use ($country): bool {
                return (
                    (int)$item->nPosTyp === \C_WARENKORBPOS_TYP_ARTIKEL
                    && $item->Artikel !== null
                    && $this->getCustomShippingCostType($item->Artikel) !== ProductDependentShippingCosts::NONE
                    && $item->Artikel->isUsedForShippingCostCalculation($country) === false
                );
            }
        );

        if (\count($cartItemsWithCustomCosts) === 0) {
            return [];
        }

        $dependentShippingMethod = $this->getDependentShippingMethod($country, $cartItemsWithCustomCosts);
        if ($dependentShippingMethod === null) {
            return [];
        }

        foreach ($cartItemsWithCustomCosts as $item) {
            if ($item->Artikel === null) {
                continue;
            }
            $shippingItem = $this->getCustomShippingCosts(
                $country,
                $item->Artikel,
                (float)$item->nAnzahl,
                $dependentShippingMethod,
            );

            if ($shippingItem !== null) {
                $result[] = $shippingItem;
            }
        }

        return $result;
    }

    /**
     * @return object{qty: float, price: float, country: string}|null
     * @comment The logic comes from legacy code. Could be refactored with a more flexibel approach (regex?).
     * @todo Should we log warnings instead of allowing fatals to be thrown due to wrong formatted custom bulk prices?
     */
    private function extractCustomShippingPrices(
        Artikel $product,
        ProductDependentShippingCosts $customCostType,
        string $country,
        float $productQty,
    ): ?object {
        $shippingData = \array_filter(
            \explode(
                ';',
                \str_replace(
                    ',',
                    '.',
                    \trim(
                        $product->FunktionsAttribute[
                        $customCostType === ProductDependentShippingCosts::FIX
                            ? \FKT_ATTRIBUT_VERSANDKOSTEN
                            : \FKT_ATTRIBUT_VERSANDKOSTEN_GESTAFFELT
                        ] ?? ''
                    )
                )
            )
        );

        foreach ($shippingData as $shipping) {
            // $shippingData wenn gestaffelt: DE 1-45,00:2-60,00:3-80;AT 1-90,00:2-120,00:3-150,00
            // $shippingData wenn normal    : DE 600,00;AT 600,00
            $data = \explode(' ', $shipping);
            if (\count($data) !== 2 || $country !== $data[0]) {
                continue;
            }
            foreach (\explode(':', $data[1]) as $shippingPrice) {
                if (\str_contains($shippingPrice, '-') === false) {
                    return (object)[
                        'qty'     => $productQty,
                        'price'   => $productQty * (float)$shippingPrice,
                        'country' => $data[0],
                    ];
                }
                $bulkPrice = \explode('-', $shippingPrice);
                $qty       = (float)$bulkPrice[0];
                $price     = (float)$bulkPrice[1];

                if ($price >= 0 && $qty > 0 && $qty <= $productQty) {
                    $result = (object)[
                        'qty'     => $productQty,
                        'price'   => $price,
                        'country' => $data[0],
                    ];
                }
            }

            return $result ?? null;
        }

        return null;
    }

    private function getCustomShippingCosts(
        string $country,
        Artikel $product,
        float $productQty,
        ShippingDTO $methodForCustomCostProducts,
    ): ?ShippingCartPositionDTO {
        $customCostType = $this->getCustomShippingCostType($product);
        $hookReturn     = false;
        \executeHook(\HOOK_TOOLS_GLOBAL_GIBARTIKELABHAENGIGEVERSANDKOSTEN, [
            'oArtikel'    => &$product,
            'cLand'       => &$country,
            'nAnzahl'     => &$productQty,
            'bHookReturn' => &$hookReturn
        ]);
        if (
            $hookReturn
            || $customCostType === ProductDependentShippingCosts::NONE
        ) {
            return null;
        }

        $customShippingCost = $this->extractCustomShippingPrices(
            $product,
            $customCostType,
            $country,
            $productQty,
        );

        if ($customShippingCost === null) {
            return null;
        }

        $namesLocalized = [];
        foreach ($this->getAvailableLanguages() as $language) {
            $namesLocalized[$language->getCode()] =
                $this->getLanguageHelper()->get('shippingFor', 'checkout')
                . ' '
                . $product->cName
                . ' (' . $customShippingCost->country . ')';
        }
        $netPrice   = $methodForCustomCostProducts->includeTaxes
            ? $this->calculateNetPrice(
                $customShippingCost->price,
                $product->kSteuerklasse
            )
            : $customShippingCost->price;
        $grossPrice = $methodForCustomCostProducts->includeTaxes
            ? $customShippingCost->price
            : $this->calculateGrossPrice(
                $customShippingCost->price,
                $product->kSteuerklasse
            );

        return ShippingCartPositionDTO::fromObject(
            (object)[
                'productID'      => $product->kArtikel ?? 0,
                'nameLocalized'  => $namesLocalized,
                'taxClassID'     => $product->kSteuerklasse,
                'netPrice'       => $netPrice,
                'priceLocalized' => $this->customerGroup->isMerchant()
                    ? Preise::getLocalizedPriceString(
                        $netPrice,
                        $this->currency
                    )
                    . ' ' . $this->getLanguageHelper()->get('plus', 'productDetails')
                    . ' ' . $this->getLanguageHelper()->get('vat', 'productDetails')
                    : Preise::getLocalizedPriceString(
                        $grossPrice,
                        $this->currency
                    ),
            ]
        );
    }

    public function getTaxRate(?int $taxClassID = null, ?string $country = null): float
    {
        if ($taxClassID === null) {
            throw new RuntimeException(
                'Can not get tax rate without a valid tax class ID'
            );
        }
        $taxRate = $this->session->getTaxRateByTaxClassID($taxClassID);
        if ($taxRate === null) {
            $taxRate = Tax::getSalesTax(
                $taxClassID,
                $country ?? $this->deliveryCountryCode
            );
            if (\is_numeric($taxRate) === false) {
                throw new RuntimeException(
                    'Could not get tax rate for tax class ID ' . $taxClassID
                );
            }
            $taxRate = (float)$taxRate;
        }

        return $taxRate;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getShippingClasses()
     * @param CartItem[]|null $cartItems
     * @return int[]
     */
    public function getShippingClasses(?array $cartItems = null): array
    {
        $result = [];
        foreach ($cartItems ?? $this->cart->PositionenArr as $item) {
            if (
                (int)$item->nPosTyp === \C_WARENKORBPOS_TYP_ARTIKEL
                && $item->kVersandklasse > 0
            ) {
                $result[$item->kVersandklasse] = $item->kVersandklasse;
            }
        }
        $result = \array_values($result);
        \sort($result);

        return $result;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getAdditionalFees()
     */
    public function getShippingSurcharge(
        ShippingDTO $shippingMethod,
        string $country,
        string $zipCode
    ): ?ShippingSurchargeDTO {
        $shippingSurcharge = $this->cache->getShippingSurcharge(
            $shippingMethod->id,
            $country,
            $zipCode,
        );
        if ($shippingSurcharge !== null) {
            return $shippingSurcharge;
        }
        $shippingSurcharge = $this->database->getSurchargeForShippingMethod(
            $shippingMethod->id,
            $country,
            $zipCode,
            $this->getAvailableLanguages(),
        );
        if ($shippingSurcharge === null) {
            return null;
        }

        $surchargeNetPrice = $shippingMethod->includeTaxes
            ? $this->calculateNetPrice($shippingSurcharge->netSurcharge)
            : $shippingSurcharge->netSurcharge;

        $shippingSurcharge->netSurcharge       = $surchargeNetPrice;
        $shippingSurcharge->surchargeLocalized = Preise::getLocalizedPriceString(
            $this->customerGroup->isMerchant() ?
                $surchargeNetPrice
                : $this->calculateGrossPrice(
                    $surchargeNetPrice,
                    $this->getTaxRateIDs(
                        $this->cart->PositionenArr,
                        $country
                    )[0]->taxRateID ?? null
                ),
            $this->currency
        );

        $shippingSurcharge = ShippingSurchargeDTO::fromObject($shippingSurcharge);
        $this->cache->setShippingSurcharge(
            $shippingMethod->id,
            $country,
            $zipCode,
            $shippingSurcharge,
        );

        return $shippingSurcharge;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\calculateShippingFees()
     * @comment $additionalProduct is only needed for hook execution and should be deprecated.
     */
    public function calcCostForShippingMethod(
        ShippingDTO $shippingMethod,
        string $iso,
        ?Artikel $product = null,
        ?Artikel $additionalProduct = null
    ): float {
        $iso       = $iso ?: $this->deliveryCountryCode;
        $cartItems = $product === null
            ? $this->cart->PositionenArr
            : [$this->cartItemFromProduct($product)];
        $netPrice  = $this->getCostsDependingOnCalcMethod(
            $shippingMethod,
            $iso,
            $cartItems,
        );

        \executeHook(\HOOK_CALCULATESHIPPINGFEES, [
            'price'             => &$netPrice,
            'shippingMethod'    => $shippingMethod->toVersandart($iso),
            'iso'               => $iso,
            'additionalProduct' => $additionalProduct,
            'product'           => $product,
        ]);

        if ($shippingMethod->dependentShipping) {
            foreach ($cartItems as $cartItem) {
                if (
                    $cartItem->Artikel === null
                    || $cartItem->Artikel->isUsedForShippingCostCalculation($iso)
                ) {
                    continue;
                }
                $netPrice += $this->getCustomShippingCostsByProduct(
                    $iso,
                    $cartItem->Artikel,
                    (int)$cartItem->nAnzahl,
                    $shippingMethod,
                )->netPrice ?? 0.00;
            }
        }

        // @todo Is it correct to include custom costs before limiting shipping price with maxPrice?
        if ($shippingMethod->maxPrice > 0) {
            $netPrice = \min(
                $netPrice,
                $shippingMethod->includeTaxes
                    ? $this->calculateNetPrice($shippingMethod->maxPrice)
                    : $shippingMethod->maxPrice
            );
        }
        // @todo Is it correct to include surcharges after limiting shipping price with maxPrice?
        $netPrice += $shippingMethod->shippingSurcharge->netSurcharge ?? 0.00;

        if ($shippingMethod->freeShippingMinAmount > 0) {
            $totalItemsValue = match ($product === null) {
                true  => $this->cart->gibGesamtsummeWarenExt(
                    [\C_WARENKORBPOS_TYP_ARTIKEL],
                    $shippingMethod->includeTaxes,
                    $shippingMethod->dependentShipping === false,
                    $iso
                ),
                false => $this->getProductPrice($product, $shippingMethod),
            };
            if ($totalItemsValue >= $shippingMethod->freeShippingMinAmount) {
                $netPrice = 0;
            }
        }
        \executeHook(\HOOK_TOOLSGLOBAL_INC_BERECHNEVERSANDPREIS, [
            'fPreis'         => &$netPrice,
            'versandart'     => $shippingMethod->toVersandart($iso),
            'cISO'           => $iso,
            'oZusatzArtikel' => $additionalProduct,
            'Artikel'        => $product,
        ]);

        return $netPrice;
    }

    public function getProductPrice(Artikel $product, ShippingDTO $shippingMethod): float
    {
        if ($product->Preise === null) {
            $product->holPreise(
                $this->customerGroup->getID(),
                $product,
                $this->customer->getID(),
            );
        }

        return $shippingMethod->includeTaxes
            ? $product->Preise->fVK[1] ?? 0.00
            : $product->Preise->fVK[0] ?? 0.00;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getLowestShippingFees()
     * @comment Only used during export. Called in includes/src/Export/Product.php
     */
    public function getLowestShippingFeesForProduct(
        string $iso,
        Artikel $product,
        bool $allowCash,
    ): float {
        $fee     = 99999;
        $iso     = $iso ?: $this->deliveryCountryCode;
        $methods = $this->database->getShippingMethods(
            $this->customerGroup->getID(),
            $this->getAvailableLanguages(),
            $iso,
        );
        foreach ($methods as $method) {
            if ($method->exclFromCheapestCalc) {
                continue;
            }
            if (
                $allowCash === false
                && $method->acceptsCashPayment
            ) {
                continue;
            }

            $shippingFee = $this->calcCostForShippingMethod(
                $method,
                $iso,
                $product,
            );

            if ($shippingFee !== -1.00 && $shippingFee < $fee) {
                $fee = $shippingFee;
            }
            if ($this->isZero($shippingFee)) {
                break;
            }
        }

        return $fee === 99999 ? -1.00 : $fee;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getDeliverytimeEstimationText()
     * @comment Would be nice to remove these constants or replace them with settings.
     */
    public function getDeliverytimeEstimationText(
        int $minDeliveryDays,
        int $maxDeliveryDays,
        int $daysToWeeksLimit = \DELIVERY_TIME_DAYS_TO_WEEKS_LIMIT,
        int $daysToMonthsLimit = \DELIVERY_TIME_DAYS_TO_MONTHS_LIMIT,
        int $daysPerWeek = \DELIVERY_TIME_DAYS_PER_WEEK,
        int $daysPerMonth = \DELIVERY_TIME_DAYS_PER_MONTH,
    ): string {
        switch (true) {
            case ($maxDeliveryDays < $daysToWeeksLimit):
                $minDelivery = $minDeliveryDays;
                $maxDelivery = $maxDeliveryDays;
                $languageVar = $minDeliveryDays === $maxDeliveryDays
                    ? 'deliverytimeEstimationSimple'
                    : 'deliverytimeEstimation';
                break;
            case ($maxDeliveryDays < $daysToMonthsLimit):
                $minDelivery = (int)\ceil($minDeliveryDays / $daysPerWeek);
                $maxDelivery = (int)\ceil($maxDeliveryDays / $daysPerWeek);
                $languageVar = $minDelivery === $maxDelivery
                    ? 'deliverytimeEstimationSimpleWeeks'
                    : 'deliverytimeEstimationWeeks';
                break;
            default:
                $minDelivery = (int)\ceil($minDeliveryDays / $daysPerMonth);
                $maxDelivery = (int)\ceil($maxDeliveryDays / $daysPerMonth);
                $languageVar = $minDelivery === $maxDelivery
                    ? 'deliverytimeEstimationSimpleMonths'
                    : 'deliverytimeEstimationMonths';
        }

        $deliveryText = $this->getDeliveryText($minDelivery, $maxDelivery, $languageVar);

        \executeHook(\HOOK_GET_DELIVERY_TIME_ESTIMATION_TEXT, [
            'min'  => $minDeliveryDays,
            'max'  => $maxDeliveryDays,
            'text' => &$deliveryText
        ]);

        return $deliveryText;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getShippingFreeString()
     */
    public function getShippingFreeString(ShippingDTO $method, string $country = ''): string
    {
        $returnEmptyString = (
            $this->session->isset('oVersandfreiKupon')
            && (
                $this->session->isset('Warenkorb') === false
                && $this->session->isset('Steuerland') === false
            )
        );
        if (
            $returnEmptyString
            || (
                $method->freeShippingMinAmount <= 0
                && $method->finalNetCost > 0
            )
        ) {
            return '';
        }

        $shippingNetFreeDifference = $this->getNetShippingFreeDifference(
            $method,
            $country ?: $this->deliveryCountryCode
        );
        if ($shippingNetFreeDifference <= 0) {
            return \sprintf(
                $this->getLanguageHelper()->get('noShippingCostsReached', 'basket'),
                $method->localizedNames[$this->getLanguageHelper()->getLanguageCode()] ?? '',
                $this->getShippingFreeCountriesString($method)
            );
        }

        return \sprintf(
            $this->getLanguageHelper()->get('noShippingCostsAt', 'basket'),
            Preise::getLocalizedPriceString(
                $this->calculateGrossPrice($shippingNetFreeDifference),
                $this->currency
            ) . $this->customerGroup->isMerchant()
                ? ' (' . $this->getLanguageHelper()->get('gross') . ')'
                : '',
            $method->localizedNames[$this->getLanguageHelper()->getLanguageCode()] ?? '',
            $this->getShippingFreeCountriesString($method)
        );
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getShippingFreeDifference()
     */
    public function getNetShippingFreeDifference(ShippingDTO $method, string $country = ''): float
    {
        $freeShippingMinAmount = $this->isZero($method->finalNetCost)
            ? $method->finalNetCost
            : $method->freeShippingMinAmount;

        return match ($method->includeTaxes) {
            true  => $this->calculateNetPrice(
                $freeShippingMinAmount - $this->cart->gibGesamtsummeWarenExt(
                    [\C_WARENKORBPOS_TYP_ARTIKEL],
                    true,
                    $method->dependentShipping === false,
                    $country ?: $this->deliveryCountryCode
                )
            ),
            false => $freeShippingMinAmount - $this->cart->gibGesamtsummeWarenExt(
                [\C_WARENKORBPOS_TYP_ARTIKEL],
                false,
                $method->dependentShipping === false,
                $country ?: $this->deliveryCountryCode
            ),
        };
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getShippingFreeCountriesString()
     */
    public function getShippingFreeCountriesString(ShippingDTO $shippingMethod): string
    {
        if ($shippingMethod->freeShippingMinAmount <= 0) {
            return '';
        }

        $langID                = $this->getLanguageHelper()->getLanguageID();
        $freeShippingCountries = $this->cache->getFreeShippingCountries(
            $shippingMethod,
            $langID
        )
            ?: $this->cache->setFreeShippingCountries(
                $shippingMethod,
                $langID
            );

        if ($freeShippingCountries === []) {
            $freeShippingCountries = $this->getCountryService()
                ->getFilteredCountryList($shippingMethod->countries)
                ->map(
                    static function ($country) use ($langID): string {
                        return $country instanceof Country
                            ? $country->getName($langID)
                            : '';
                    }
                )->all();
        }

        return \implode(', ', $freeShippingCountries ?? []);
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getFreeShippingMinimum()
     */
    public function getFreeShippingMethod(
        string $countryISOCode = '',
        string $zipCode = '',
    ): ?ShippingDTO {
        return $this->getPossibleFreeShippingMethods(
            $countryISOCode ?: $this->deliveryCountryCode,
            $zipCode ?: $this->deliveryZipCode,
        )[0] ?? null;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getPossibleShippingCountries()
     * @param string[] $filterISO
     * @return Country[]
     */
    public function getPossibleShippingCountries(
        array $filterISO = [],
        int $customerGroupID = 0,
    ): array {
        /** @var Country[] $countries */
        $countries = $this->getCountryService()->getFilteredCountryList(
            $this->database->filterCountriesByISOCode(
                $filterISO,
                $customerGroupID ?: $this->customerGroup->getID(),
                $this->getShippingClasses(),
                $this->cartIsDependent(),
            )
        )->toArray();

        \executeHook(\HOOK_TOOLSGLOBAL_INC_GIBBELIEFERBARELAENDER, [
            'oLaender_arr' => &$countries
        ]);

        return $countries;
    }

    /**
     * @former JTL\Helpers\ShippingMethod\getPossiblePackagings()
     * @return array<int, object{kVerpackung: int, kSteuerklasse: int, cName: string, cKundengruppe: string,
     *     fBrutto: float, fMindestbestellwert: float, fKostenfrei: float, nAktiv: int, kVerpackungSprache: int,
     *     cISOSprache: string, cBeschreibung: string, fBruttoLocalized: string, nKostenfrei: int,
     *     fBruttoLocalized: float, nKostenfrei: 1|0}>
     * @todo Another DTO would be nice.
     */
    public function getPossiblePackagings(): array
    {
        $result  = [];
        $cartSum = $this->cart->gibGesamtsummeWarenExt([\C_WARENKORBPOS_TYP_ARTIKEL], true);
        foreach (
            $this->database->getPackagings(
                $this->getLanguageHelper()->getLanguageCode(),
                $this->customerGroup->getID(),
                $cartSum,
            ) as $packaging
        ) {
            $result[] = (object)\array_merge((array)$packaging, [
                'fBruttoLocalized' => Preise::getLocalizedPriceString(
                    $packaging->fBrutto,
                    $this->currency,
                ),
                'nKostenfrei'      => (
                    $cartSum >= $packaging->fKostenfrei
                    && $packaging->fBrutto > 0
                    && $packaging->fKostenfrei > 0
                )
                    ? 1
                    : 0,
            ]);
        }

        return $result;
    }

    /**
     * @param ShippingDTO[] $shippingMethods
     */
    public function getShippingByPaymentMethodID(
        array $shippingMethods,
        int $paymentMethodID,
    ): ?ShippingDTO {
        if ($paymentMethodID > 0 && empty($shippingMethods) === false) {
            $shippingMethods = \array_values(
                \array_filter(
                    $shippingMethods,
                    function (ShippingDTO $method) use ($paymentMethodID) {
                        return $this->filterPaymentMethodByID(
                            $this->getPaymentMethodsByShipping($method->id),
                            $paymentMethodID
                        ) !== null;
                    }
                )
            );
        }

        return $shippingMethods[0] ?? null;
    }

    public function getPaymentMethodID(): int
    {
        return $this->session->getPaymentMethod()->kZahlungsart ?? 0;
    }

    /**
     * @comment May return gross or net price.
     */
    private function getBulkPrice(ShippingDTO $shippingMethod, float $till): ?float
    {
        if (\count($shippingMethod->bulkPrices) === 0) {
            return $this->database->getBulkPrice(
                $shippingMethod->id,
                $till,
            )->price ?? null;
        }

        $result = null;
        foreach ($shippingMethod->bulkPrices as $bulkPrice) {
            if ($bulkPrice->till >= $till) {
                $result = $bulkPrice->price;
                break;
            }
        }

        return $result;
    }

    /**
     * @param CartItem[] $cartItems
     * @description Returns shipping costs depending on the calculation method or -1.00 if the method is not supported.
     */
    private function getCostsDependingOnCalcMethod(
        ShippingDTO $shippingMethod,
        string $iso,
        array $cartItems
    ): float {
        $result = match ($shippingMethod->calculationType) {
            ShippingCalculationMethod::VM_VERSANDKOSTEN_PAUSCHALE_JTL         => $shippingMethod->price,
            ShippingCalculationMethod::VM_VERSANDBERECHNUNG_GEWICHT_JTL       =>
            $this->getBulkPrice(
                $shippingMethod,
                (float)\array_sum(
                    \array_map(
                        static function (CartItem $cartItem) use ($iso, $shippingMethod): float {
                            if (
                                $cartItem->Artikel === null
                                || $cartItem->Artikel->isUsedForShippingCostCalculation($iso)
                                === $shippingMethod->dependentShipping
                            ) {
                                return 0.00;
                            }

                            return (float)$cartItem->fGesamtgewicht;
                        },
                        $cartItems
                    )
                )
            ),
            ShippingCalculationMethod::VM_VERSANDBERECHNUNG_WARENWERT_JTL     =>
            $this->getBulkPrice(
                $shippingMethod,
                (float)\array_sum(
                    \array_map(
                        static function (CartItem $cartItem) use ($iso, $shippingMethod): float {
                            if (
                                $cartItem->Artikel === null
                                || $cartItem->Artikel->isUsedForShippingCostCalculation($iso)
                                    === $shippingMethod->dependentShipping
                            ) {
                                return 0.00;
                            }

                            $totalNetPrice = (float)(
                                    $cartItem->fPreisEinzelNetto ?? 0.00
                                ) * (float)(
                                    $cartItem->nAnzahl ?? 0.00
                                );

                            return $shippingMethod->includeTaxes
                                ? $totalNetPrice * ((CartItem::getTaxRate($cartItem) / 100) + 1)
                                : $totalNetPrice;
                        },
                        $cartItems
                    )
                )
            ),
            ShippingCalculationMethod::VM_VERSANDBERECHNUNG_ARTIKELANZAHL_JTL =>
            $this->getBulkPrice(
                $shippingMethod,
                (float)\array_sum(
                    \array_map(
                        static function (CartItem $cartItem) use ($iso, $shippingMethod): int {
                            if (
                                $cartItem->Artikel === null
                                || $cartItem->Artikel->isUsedForShippingCostCalculation($iso)
                                === $shippingMethod->dependentShipping
                            ) {
                                return 0;
                            }

                            return $cartItem->istKonfig() === false
                                ? (int)$cartItem->nAnzahl
                                : 0;
                        },
                        $cartItems
                    )
                )
            ),
        };

        if ($result === null) {
            return -1.00;
        }

        return $shippingMethod->includeTaxes
            ? $this->calculateNetPrice(
                $result,
                $this->getTaxRateIDs(
                    $cartItems,
                    $iso,
                )[0]->taxRateID ?? null
            )
            : $result;
    }

    public function calculateNetPrice(float $grossPrice, ?int $taxClassID = null): float
    {
        return $grossPrice / (
            100 + $this->getTaxRate(
                $taxClassID
                    ?? $this->getTaxRateIDs($this->cart->PositionenArr)[0]->taxRateID
                    ?? null
            )) * 100;
    }

    public function calculateGrossPrice(float $netPrice, ?int $taxClassID = null): float
    {
        return Tax::getGross(
            $netPrice,
            $this->getTaxRate(
                $taxClassID
                    ?? $this->getTaxRateIDs($this->cart->PositionenArr)[0]->taxRateID
                    ?? null
            )
        );
    }

    /**
     * @param CartItem[] $cartItemsWithCustomCosts
     */
    public function getDependentShippingMethod(string $country, array $cartItemsWithCustomCosts): ?ShippingDTO
    {
        $hash                     = $this->getCartChecksum($cartItemsWithCustomCosts, $country);
        $dependentShippingMethods = $this->session->getPossibleDependentMethods(
            $country,
            $hash,
        );
        if (\count($dependentShippingMethods) === 0) {
            $dependentShippingMethods = \array_filter(
                $this->getMethodsFromCacheOrDB(
                    $country,
                    $this->customerGroup->getID(),
                ),
                static function (ShippingDTO $method): bool {
                    return $method->dependentShipping;
                },
            );
            $dependentShippingMethods = $this->filterMethodsByCartItems(
                $dependentShippingMethods,
                $country,
                $cartItemsWithCustomCosts,
            );
            $this->session->setPossibleDependentMethods(
                $dependentShippingMethods,
                $country,
                $hash,
            );
        }

        $dependentShippingMethod = \array_reduce(
            $dependentShippingMethods,
            static function (?ShippingDTO $carry, ShippingDTO $method): ?ShippingDTO {
                return $method->dependentShipping ? $method : $carry;
            },
        );

        return $dependentShippingMethod instanceof ShippingDTO
            ? $dependentShippingMethod
            : null;
    }

    /**
     * @param ShippingDTO[] $possibleMethods
     * @param CartItem[]    $cartItems
     * @return ShippingDTO[]
     */
    private function filterMethodsByCartItems(
        array $possibleMethods,
        string $deliveryCountryCode,
        array $cartItems
    ): array {
        $result               = [];
        $shippingClasses      = $this->getShippingClasses($cartItems);
        $onlyDependentMethods = $this->cartIsDependent(
            $deliveryCountryCode,
            $cartItems,
        );

        foreach ($possibleMethods as $shippingMethod) {
            if ($onlyDependentMethods !== $shippingMethod->dependentShipping) {
                continue;
            }
            if (\in_array('-1', $shippingMethod->allowedShippingClasses, true)) {
                $result[] = $shippingMethod;
                continue;
            }
            foreach ($shippingMethod->allowedShippingClasses as $allowedClassesCombination) {
                $allowedShippingClasses = \explode('-', $allowedClassesCombination);
                $shippingClassCount     = \count($shippingClasses);
                if (\count($allowedShippingClasses) !== $shippingClassCount) {
                    continue;
                }
                if (
                    \count(
                        \array_intersect(
                            \array_map(
                                '\intval',
                                $allowedShippingClasses
                            ),
                            $shippingClasses
                        )
                    ) === $shippingClassCount
                ) {
                    $result[] = $shippingMethod;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @return ShippingDTO[]
     */
    private function getMethodsFromCacheOrDB(
        string $deliveryCountryCode,
        int $customerGroupID,
    ): array {
        $possibleMethods = $this->cache->getPossibleMethods(
            $deliveryCountryCode,
            $customerGroupID,
        );
        if ($possibleMethods === []) {
            $possibleMethods = $this->database->getShippingMethods(
                $customerGroupID,
                $this->getAvailableLanguages(),
                $deliveryCountryCode,
            );
            if ($possibleMethods === []) {
                return [];
            }
            $this->cache->setPossibleMethods(
                $possibleMethods,
                $deliveryCountryCode,
                $customerGroupID,
            );
        }

        return $possibleMethods;
    }

    /**
     * @todo Review this method and take care of config items, variations and so on.
     */
    private function cartItemFromProduct(Artikel $product): CartItem
    {
        $unique                      = '';
        $cartItem                    = new CartItem();
        $cartItem->Artikel           = $product;
        $cartItem->nAnzahl           = 1;
        $cartItem->kArtikel          = $product->kArtikel;
        $cartItem->kVersandklasse    = $product->kVersandklasse ?? 0;
        $cartItem->kSteuerklasse     = $product->kSteuerklasse ?? 0;
        $cartItem->fPreisEinzelNetto = $product->Preise->fVKNetto ?? 0.00;
        $cartItem->fPreis            = $cartItem->fPreisEinzelNetto;
        $cartItem->cArtNr            = $product->cArtNr ?? '';
        $cartItem->nPosTyp           = \C_WARENKORBPOS_TYP_ARTIKEL;
        $cartItem->cEinheit          = $product->cEinheit ?? '';
        $cartItem->cUnique           = $unique;
        $cartItem->cResponsibility   = 'core';
        $cartItem->kKonfigitem       = 0;
        $cartItem->cName             = [];
        $cartItem->cLieferstatus     = [];
        $cartItem->fVK               = $product->Preise->fVK ?? [];
        $cartItem->fGesamtgewicht    = $cartItem->gibGesamtgewicht();

        if ($product->isKonfigItem) {
            $cartItem->cUnique     = \uniqid('', true);
            $cartItem->kKonfigitem = $product->kArtikel;
        }

        return $cartItem;
    }

    /**
     * @param CartItem[] $cartItems
     */
    private function getPredominantTaxRate(array $cartItems): int
    {
        $taxRates = [];
        foreach ($cartItems as $item) {
            if (
                $item->nPosTyp === \C_WARENKORBPOS_TYP_ARTIKEL
                && $item->kSteuerklasse > 0
            ) {
                $taxRates[$item->kSteuerklasse] = ($taxRates[$item->kSteuerklasse] ?? 0.00)
                    + ((float)$item->fPreisEinzelNetto * (int)$item->nAnzahl);
            }
        }

        return empty($taxRates) === false
            ? (int)\array_search(
                \max($taxRates),
                $taxRates,
                true
            )
            : 0;
    }

    /**
     * @param CartItem[] $cartItems
     */
    private function getHighestTaxRate(array $cartItems, ?string $country = null): int
    {
        $rate      = -1;
        $taxRateID = 0;
        foreach ($cartItems as $item) {
            if (
                $item->nPosTyp === \C_WARENKORBPOS_TYP_ARTIKEL
                && $item->kSteuerklasse > 0
                && $this->getTaxRate($item->kSteuerklasse, $country) > $rate
            ) {
                $rate      = $this->getTaxRate($item->kSteuerklasse, $country);
                $taxRateID = $item->kSteuerklasse;
            }
        }

        return $taxRateID;
    }

    /**
     * @param CartItem[] $cartItems
     * @return array<int, object{taxRateID: int, proportion: float}>
     */
    private function getProportionalTaxRates(array $cartItems): array
    {
        $result    = [];
        $taxRates  = [];
        $cartValue = 0.00;
        foreach ($cartItems as $item) {
            if (
                $item->nPosTyp === \C_WARENKORBPOS_TYP_ARTIKEL
                && $item->kSteuerklasse > 0
            ) {
                $itemValue                      = (float)$item->fPreisEinzelNetto * (int)$item->nAnzahl;
                $cartValue                      += $itemValue;
                $taxRates[$item->kSteuerklasse] = ($taxRates[$item->kSteuerklasse] ?? 0.00)
                    + $itemValue;
            }
        }
        foreach ($taxRates as $taxRateID => $itemValueInCart) {
            $result[] = (object)[
                'taxRateID'  => $taxRateID,
                'proportion' => \round(
                    ($itemValueInCart * 100) / $cartValue,
                    2
                ),
            ];
        }

        return $result;
    }

    /**
     * @param CartItem[]|null $cartItems
     * @return array<int, object{taxRateID: int, proportion: float}>
     * @todo Before calling this method, filter out dependent cart items if the used shipping method is not dependent.
     */
    public function getTaxRateIDs(?array $cartItems = null, ?string $country = null): array
    {
        $cartItems ??= $this->cart->PositionenArr;

        $result = match ($this->config[Checkout::SHIPPING_TAX_RATE->getValue()]) {
            'US' => [
                (object)[
                    'taxRateID'  => $this->getPredominantTaxRate($cartItems),
                    'proportion' => 100.00,
                ]
            ],
            'HS' => [
                (object)[
                    'taxRateID'  => $this->getHighestTaxRate($cartItems, $country),
                    'proportion' => 100.00,
                ]
            ],
            'PS' => $this->getProportionalTaxRates($cartItems),
            default => [],
        };

        return $result;
    }

    /**
     * @param ShippingDTO[] $possibleMethods
     * @return ShippingDTO[]
     */
    private function setShippingMethodPrices(
        array $possibleMethods,
        float $itemTotalValue,
        string $country,
        Artikel $product,
        CartItem $cartItem,
    ): array {
        $result = [];
        foreach ($possibleMethods as $method) {
            $shippingNetFees = $method->dependentShipping
                ? $this->getCustomShippingCostsByProduct(
                    $country,
                    $product,
                    (float)$cartItem->nAnzahl,
                    $method,
                )->netPrice ?? -1.00
                : $this->getCostsDependingOnCalcMethod(
                    $method,
                    $country,
                    [$cartItem],
                );

            if ($shippingNetFees === -1.00) {
                continue;
            }
            $shippingNetFees = $this->considerMaxPriceAndFreeShipping($method, $shippingNetFees, $itemTotalValue);
            $localizedPrice  = $this->customerGroup->isMerchant()
                ? Preise::getLocalizedPriceString(
                    $shippingNetFees,
                    $this->currency,
                )
                : Preise::getLocalizedPriceString(
                    $this->calculateGrossPrice(
                        $shippingNetFees,
                        $product->kSteuerklasse
                    ),
                    $this->currency
                );

            $result[] = $method
                ->setPrices(
                    (object)[
                        'finalNetCost'   => $shippingNetFees,
                        'finalGrossCost' => $this->calculateGrossPrice(
                            $shippingNetFees,
                            $product->kSteuerklasse
                        ),
                        'localizedPrice' => match ($this->isZero($shippingNetFees)) {
                            true  => $this->getLanguageHelper()->get('freeshipping'),
                            false => $localizedPrice . $this->getVatNote(),
                        },
                    ]
                );
        }

        return $result;
    }

    private function considerMaxPriceAndFreeShipping(
        ShippingDTO $method,
        float $shippingNetFees,
        float $itemTotalValue
    ): float {
        if ($method->maxPrice > 0) {
            $maxPrice = $method->includeTaxes
                ? $this->calculateNetPrice($method->maxPrice)
                : $method->maxPrice;
            if ($shippingNetFees >= $maxPrice) {
                $shippingNetFees = $maxPrice;
            }
        }

        if ($method->freeShippingMinAmount > 0) {
            $freeShippingMinAmount = $method->includeTaxes
                ? $this->calculateNetPrice($method->freeShippingMinAmount)
                : $method->freeShippingMinAmount;
            if ($itemTotalValue >= $freeShippingMinAmount) {
                $shippingNetFees = 0.00;
            }
        }

        return $shippingNetFees;
    }

    /**
     * @param ShippingDTO[]|null $shippingMethods
     */
    public function getFirstShippingMethod(?array $shippingMethods = null, int $paymentMethodID = 0): ?ShippingDTO
    {
        $shippingMethods ??= $this->getPossibleShippingMethods();

        return $this->getShippingByPaymentMethodID(
            $shippingMethods,
            $paymentMethodID ?: $this->getPaymentMethodID(),
        );
    }

    /**
     * @return ShippingDTO[]
     */
    public function getSortedPossibleShippings(): array
    {
        return $this->sortMethodsByPrice(
            $this->getPossibleShippingMethods()
        );
    }
}
