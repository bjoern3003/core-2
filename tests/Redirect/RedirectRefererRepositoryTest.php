<?php

declare(strict_types=1);

namespace Tests\Redirect;

use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use JTL\Redirect\DomainObjects\RedirectRefererDomainObject;
use JTL\Redirect\Repositories\RedirectRefererRepository;
use Tests\UnitTestCase;

class RedirectRefererRepositoryTest extends UnitTestCase
{
    private DbInterface $db;

    private RedirectRefererRepository $repository;

    public function setUp(): void
    {
        $this->db         = $this->createStub(NiceDB::class);
        $this->repository = new RedirectRefererRepository($this->db);
    }

    public function testGetReferers(): void
    {
        $this->assertCount(0, $this->repository->getReferers(1, 3));
        $this->db->method('getObjects')
            ->willReturn([
                (object)[
                    'kRedirectReferer'  => '12345',
                    'kBesucherBot'      => '1',
                    'kRedirect'         => '1',
                    'cRefererUrl'       => 'url',
                    'cIP'               => '192.168.1.1',
                    'dDate'             => '1529201514',
                    'cBesucherBotName'  => 'botName',
                    'cBesucherBotAgent' => 'botAgent',
                ],
                (object)[
                    'kRedirectReferer'  => '12346',
                    'kBesucherBot'      => '1',
                    'kRedirect'         => '1',
                    'cRefererUrl'       => '192.168.1.',
                    'cIP'               => '192.168.1.2',
                    'dDate'             => '1529201515',
                    'cBesucherBotName'  => 'botName2',
                    'cBesucherBotAgent' => 'botAgent2',
                ],
                (object)[
                    'kRedirectReferer'  => '12347',
                    'kBesucherBot'      => '1',
                    'kRedirect'         => '1',
                    'cRefererUrl'       => 'url3',
                    'cIP'               => '192.168.1.3',
                    'dDate'             => '1529201516',
                    'cBesucherBotName'  => 'botName3',
                    'cBesucherBotAgent' => 'botAgent3',
                ],
            ]);
        $res = $this->repository->getReferers(1, 3);
        $this->assertCount(3, $res);
        $this->assertEquals(12345, $res[0]->kRedirectReferer);
        $this->assertEquals(1, $res[0]->kBesucherBot);
        $this->assertEquals('192.168.1.2', $res[1]->cIP);
        $this->assertEquals(1529201516, $res[2]->dDate);
    }

    public function testGetKeyValue(): void
    {
        $dto = new RedirectRefererDomainObject(1, 2, 'test', '192.168.178.1', null);
        $this->assertNull($this->repository->getKeyValue($dto));
        $obj = $dto->toObject();
        $this->assertEquals($dto->redirectID, $obj->kRedirect);

        $dto = new RedirectRefererDomainObject(1, 2, 'test', '192.168.178.1', 1529201516, 42);
        $this->assertEquals(42, $this->repository->getKeyValue($dto));
        $obj = $dto->toObject(true);
        $this->assertEquals($dto->id, $obj->kRedirectReferer);
    }
}
