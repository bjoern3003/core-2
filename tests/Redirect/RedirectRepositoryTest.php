<?php

declare(strict_types=1);

namespace Tests\Redirect;

use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use JTL\Redirect\DomainObjects\RedirectDomainObject;
use JTL\Redirect\Repositories\RedirectRepository;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\UnitTestCase;

class RedirectRepositoryTest extends UnitTestCase
{
    private DbInterface $db;

    private RedirectRepository $repository;

    public function setUp(): void
    {
        $this->db         = $this->createStub(NiceDB::class);
        $this->repository = new RedirectRepository($this->db);
    }

    /**
     * @return \stdClass[][][]
     */
    public static function dataProviderRedirects(): array
    {
        return [
            [
                [
                    (object)[
                        'kRedirect'     => '12345',
                        'cFromUrl'      => 'from',
                        'cToUrl'        => 'to',
                        'nCount'        => '42',
                        'cAvailable'    => '',
                        'paramHandling' => '1',
                        'type'          => '3',
                        'dateCreated'   => '2024-08-05 14:19:38',
                    ],
                    (object)[
                        'kRedirect'     => '12346',
                        'cFromUrl'      => 'from2',
                        'cToUrl'        => 'to2',
                        'nCount'        => '23',
                        'cAvailable'    => 'y',
                        'paramHandling' => '0',
                        'type'          => '2',
                        'dateCreated'   => '2024-08-05 13:00:00',
                    ],
                    (object)[
                        'kRedirect'     => '12347',
                        'cFromUrl'      => 'from3',
                        'cToUrl'        => 'to3',
                        'nCount'        => '11',
                        'cAvailable'    => 'n',
                        'paramHandling' => '2',
                        'type'          => '1',
                        'dateCreated'   => '2024-08-05 12:00:00',
                    ]
                ]
            ]
        ];
    }

    public function testGetTableName(): void
    {
        $this->assertEquals('tredirect', $this->repository->getTableName());
    }

    public function testGetKeyName(): void
    {
        $this->assertEquals('kRedirect', $this->repository->getKeyName());
    }

    public function testGetItemByDestination(): void
    {
        $this->assertNull($this->repository->getItemByDestination('test'));
        $this->db->method('select')
            ->willReturn(
                (object)[
                    'kRedirect'     => '12345',
                    'cFromUrl'      => 'from',
                    'cToUrl'        => 'to',
                    'nCount'        => '42',
                    'cAvailable'    => '',
                    'paramHandling' => '1',
                    'type'          => '3',
                    'dateCreated'   => '2024-08-05 14:19:38',
                ]
            );
        $res = $this->repository->getItemByDestination('from');
        $this->assertIsObject($res);
        $this->assertEquals(12345, $res->kRedirect);
        $this->assertEquals('from', $res->cFromUrl);
    }

    public function testDeleteUnassigned(): void
    {
        $this->db->method('getAffectedRows')
            ->willReturn(23);
        $this->assertEquals(23, $this->repository->deleteUnassigned());
    }

    public function testUpdateByDestination(): void
    {
        $this->db->method('update')
            ->willReturn(1);
        $this->assertEquals(1, $this->repository->updateByDestination('test', 'test2', 0, 1));
    }

    public function testDeleteBySourceAndDestination(): void
    {
        $this->db->method('delete')
            ->willReturn(1);
        $this->assertEquals(1, $this->repository->deleteBySourceAndDestination('test', 'test2'));
    }

    public function testGetTotalCount(): void
    {
        $this->db->method('getSingleInt')
            ->willReturnOnConsecutiveCalls(42, 1);
        $this->assertEquals(42, $this->repository->getTotalCount());
        $this->assertEquals(1, $this->repository->getTotalCount('kRedirect = 1'));
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetItemsBySource(array $redirects): void
    {
        $this->db->method('getObjects')
            ->willReturn($redirects);
        $item = $this->repository->getItemsBySource('from');
        $this->assertSame(12345, $item[0]->kRedirect);
        $this->assertSame('from', $item[0]->cFromUrl);
        $this->assertSame('to', $item[0]->cToUrl);
        $this->assertSame(42, $item[0]->nCount);
        $this->assertSame(1, $item[0]->paramHandling);
        $this->assertSame(3, $item[0]->type);
        $this->assertSame('', $item[0]->cAvailable);
        $this->assertSame('2024-08-05 14:19:38', $item[0]->dateCreated);
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetItemsByDestination(array $redirects): void
    {
        $this->db->method('getObjects')
            ->willReturn($redirects);
        $item = $this->repository->getItemsByDestination('to2');
        $this->assertSame(12346, $item[1]->kRedirect);
        $this->assertSame('from2', $item[1]->cFromUrl);
        $this->assertSame('to2', $item[1]->cToUrl);
        $this->assertSame(23, $item[1]->nCount);
        $this->assertSame(0, $item[1]->paramHandling);
        $this->assertSame('y', $item[1]->cAvailable);
        $this->assertSame(2, $item[1]->type);
        $this->assertSame('2024-08-05 13:00:00', $item[1]->dateCreated);
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetItemBySource(array $redirects): void
    {
        $this->db->method('select')
            ->willReturn($redirects[0]);
        $item = $this->repository->getItemBySource('from');
        $this->assertIsObject($item);
        $this->assertSame(12345, $item->kRedirect);
        $this->assertSame('from', $item->cFromUrl);
        $this->assertSame('to', $item->cToUrl);
        $this->assertSame(42, $item->nCount);
        $this->assertSame(1, $item->paramHandling);
        $this->assertSame(3, $item->type);
        $this->assertSame('', $item->cAvailable);
        $this->assertSame('2024-08-05 14:19:38', $item->dateCreated);
    }

    public function testGetItemBySourceNull(): void
    {
        $this->db->method('select')
            ->willReturn(null);
        $this->assertNull($this->repository->getItemBySource('from'));
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetItemBySourceAndDestination(array $redirects): void
    {
        $this->db->method('select')
            ->willReturn($redirects[0]);
        $item = $this->repository->getItemBySourceAndDestination('from', 'to');
        $this->assertIsObject($item);
        $this->assertSame(12345, $item->kRedirect);
        $this->assertSame('from', $item->cFromUrl);
        $this->assertSame('to', $item->cToUrl);
        $this->assertSame(42, $item->nCount);
        $this->assertSame(1, $item->paramHandling);
        $this->assertSame(3, $item->type);
        $this->assertSame('', $item->cAvailable);
        $this->assertSame('2024-08-05 14:19:38', $item->dateCreated);
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetObjectByID(array $redirects): void
    {
        $this->db->method('select')
            ->willReturn($redirects[0]);
        $item = $this->repository->getObjectByID(12345);
        $this->assertIsObject($item);
        $this->assertSame(12345, $item->id);
        $this->assertSame('from', $item->source);
        $this->assertSame('to', $item->destination);
        $this->assertSame(42, $item->count);
        $this->assertSame(1, $item->paramHandling);
        $this->assertSame(3, $item->type);
        $this->assertSame('', $item->available);
        $this->assertSame('2024-08-05 14:19:38', $item->dateCreated);
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetRedirects(array $redirects): void
    {
        foreach ($redirects as $redirect) {
            $redirect->kRedirectReferer = 123;
            $redirect->kBesucherBot     = 1;
            $redirect->cRefererUrl      = 'test';
        }
        $this->db->method('getObjects')
            ->willReturnOnConsecutiveCalls($redirects, [], [], []);
        $items = $this->repository->getRedirects();

        $this->assertCount(3, $items);
        $this->assertIsObject($items[0]);
        $this->assertSame(12345, $items[0]->kRedirect);
        $this->assertSame(12346, $items[1]->kRedirect);
        $this->assertSame(12347, $items[2]->kRedirect);
        $this->assertObjectHasProperty('cFromUrl', $items[0]);
        $this->assertObjectHasProperty('cFromUrl', $items[1]);
        $this->assertObjectHasProperty('cFromUrl', $items[2]);
    }

    #[DataProvider('dataProviderRedirects')]
    public function testGetRedirectsLimit(array $redirects): void
    {
        foreach ($redirects as $redirect) {
            $redirect->kRedirectReferer = 123;
            $redirect->kBesucherBot     = 1;
            $redirect->cRefererUrl      = 'test';
        }
        $this->db->method('getObjects')
            ->willReturn([$redirects[0], $redirects[1]]);
        $this->assertCount(2, $this->repository->getRedirects('kRedirect = -1'));
        $this->assertCount(2, $this->repository->getRedirects('kRedirect = -1', 'kRedirect'));
        $this->assertCount(2, $this->repository->getRedirects('kRedirect = -1', 'kRedirect', '2'));
        $this->assertCount(2, $this->repository->getRedirects('', 'kRedirect', '2'));
        $this->assertCount(2, $this->repository->getRedirects('', 'kRedirect'));
        $this->assertCount(2, $this->repository->getRedirects('', '', '2'));
    }

    public function testDto(): void
    {
        $dto = new RedirectDomainObject('from', 'to', 42, '2024-08-05 14:19:38', 'y', 0, 1, 1);
        $this->assertEquals('from', $dto->source);
        $this->assertEquals('to', $dto->destination);
        $this->assertEquals(42, $dto->id);
        $this->assertEquals(42, $dto->toObject(true)->kRedirect);
        $dto = new RedirectDomainObject('from', 'to');
        $obj = $dto->toObject();
        $this->assertEquals('from', $obj->cFromUrl);
        $this->assertEquals(0, $dto->count);
        $this->assertEquals(0, $obj->nCount);
        $this->assertEquals($dto->id, $obj->kRedirect);
    }
}
