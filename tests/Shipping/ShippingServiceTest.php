<?php

declare(strict_types=1);

namespace Tests\Shipping;

use JTL\Cart\Cart;
use JTL\Cart\CartItem;
use JTL\Catalog\Product\Artikel;
use JTL\Catalog\Product\Preise;
use JTL\Checkout\Lieferadresse;
use JTL\Country\Country;
use JTL\Customer\CustomerGroup;
use JTL\Session\Frontend;
use JTL\Settings\Option\Checkout;
use JTL\Shipping\DomainObjects\PaymentDTO;
use JTL\Shipping\DomainObjects\ShippingDTO;
use JTL\Shipping\Helper\AdditionalFeeType;
use JTL\Shipping\Helper\ProductDependentShippingCosts;
use JTL\Shipping\Helper\ShippingCalculationMethod;
use JTL\Shipping\Repositories\CacheRepository;
use JTL\Shipping\Repositories\DatabaseRepository;
use JTL\Shipping\Services\ShippingService;
use JTL\Shopsetting;
use PHPUnit\Framework\MockObject\Exception;
use stdClass;
use Tests\TestHelper\DataTransferObjectTrait;
use Tests\TestHelper\LanguageHelperExtended;
use Tests\UnitTestCase;

class ShippingServiceTest extends UnitTestCase
{
    use DataTransferObjectTrait;

    private ShippingService $shippingService;

    /**
     * @param array<string, mixed> $customData
     */
    private function generateShippingMethod(array $customData = []): ShippingDTO
    {
        $data = $this->generateDataForDTO(
            ShippingDTO::class,
            \array_merge(
                [
                    'allowedShippingClasses' => ['-1'],
                    'dependentShipping'      => false,
                    'exclFromCheapestCalc'   => false,
                    'finalNetCost'           => -1.00,
                    'finalGrossCost'         => -1.00,
                    'localizedPrice'         => '',
                    'shippingSurcharge'      => null,
                    'calculationType'        => ShippingCalculationMethod::VM_VERSANDKOSTEN_PAUSCHALE_JTL,
                ],
                $customData,
            ),
        );

        return ShippingDTO::fromObject(
            (object)$data,
        );
    }

    /**
     * @param array<string, mixed> $customData
     */
    private function generatePaymentMethod(array $customData = []): PaymentDTO
    {
        $data = $this->generateDataForDTO(
            PaymentDTO::class,
            \array_merge(
                ['additionalFeeType' => AdditionalFeeType::ADDITIONAL_FEE_TYPE_FIX_PRICE],
                $customData,
            ),
        );

        return PaymentDTO::fromObject(
            (object)$data,
        );
    }

    /**
     * @param array<string, mixed> $customData
     * @return stdClass{surchargeID: int, shippingMethodID: int, isoCode: string, name: string, netSurcharge: float,
     *      surchargeLocalized: null, localizedNames: array<string, string>}
     */
    private function generateShippingSurcharge(array $customData = []): stdClass
    {
        $result = \array_merge([
            'surchargeID'        => 1,
            'shippingMethodID'   => 1,
            'isoCode'            => 'DE',
            'name'               => 'Berlin Surcharge',
            'netSurcharge'       => 3.85,
            'surchargeLocalized' => null,
            'localizedNames'     => [
                'ger' => 'Berlin Zuschlag',
                'eng' => 'Berlin surcharge',
            ],
        ], $customData);

        return (object)$result;
    }

    /**
     * @param array<string, mixed> $customData
     * @return stdClass{kVerpackung: int, kSteuerklasse: int, cName: string, cKundengruppe: string,
     *     fBrutto: float, fMindestbestellwert: float, fKostenfrei: float, nAktiv: int, kVerpackungSprache: int,
     *     cISOSprache: string, cBeschreibung: string}
     */
    private function generatePackaging(array $customData = []): stdClass
    {
        $result = \array_merge([
            'kVerpackung'         => 1,
            'kSteuerklasse'       => 1,
            'cName'               => '',
            'cKundengruppe'       => '-1',
            'fBrutto'             => 5.85,
            'fMindestbestellwert' => 0.00,
            'fKostenfrei'         => 0.00,
            'nAktiv'              => 1,
            'kVerpackungSprache'  => 1,
            'cISOSprache'         => 'DE',
            'cBeschreibung'       => 'Generated during unit testing',
        ], $customData);

        return (object)$result;
    }

    /**
     * @param array<string, mixed> $config
     * @throws Exception
     */
    private function getShippingService(?array $config = null): ShippingService
    {
        $databaseRepository = $this->createMock(DatabaseRepository::class);
        $databaseRepository->method('getShippingMethods')->willReturn(
            [
                $this->generateShippingMethod(
                    [
                        'id'                     => 1,
                        'price'                  => 12.85,
                        'allowedShippingClasses' => ['1', '1-2', '3'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 2,
                        'price'                  => 12.85,
                        'freeShippingMinAmount'  => 50.00,
                        'allowedShippingClasses' => ['1', '1-2', '3'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 3,
                        'price'                  => 8.85,
                        'freeShippingMinAmount'  => 50.00,
                        'allowedShippingClasses' => ['-1'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                        'dependentShipping'      => true,
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 4,
                        'price'                  => 8.85,
                        'freeShippingMinAmount'  => 50.00,
                        'allowedShippingClasses' => ['-1'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                        'dependentShipping'      => true,
                        'includeTaxes'           => false,
                        'calculationType'        => ShippingCalculationMethod::VM_VERSANDBERECHNUNG_GEWICHT_JTL,
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 5,
                        'price'                  => 8.85,
                        'allowedShippingClasses' => ['-1'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                        'acceptsCashPayment'     => false,
                        'includeTaxes'           => false,
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 6,
                        'price'                  => 7.85,
                        'allowedShippingClasses' => ['-1'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                        'includeTaxes'           => false,
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 7,
                        'price'                  => 7.85,
                        'allowedShippingClasses' => ['-1'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                        'freeShippingMinAmount'  => 50.00,
                    ],
                ),
                $this->generateShippingMethod(
                    [
                        'id'                     => 8,
                        'price'                  => 7.85,
                        'allowedShippingClasses' => ['-1'],
                        'countries'              => [
                            'DE',
                            'AT',
                        ],
                        'freeShippingMinAmount'  => 50.00,
                        'sort'                   => 0,
                    ],
                ),
            ],
        );
        $databaseRepository
            ->method('getFreeShippingMethods')
            ->willReturn([
                $this->generateShippingMethod(
                    [
                        'freeShippingMinAmount' => 50.00,
                        'countries' => [
                            'DE',
                            'AT',
                        ],
                    ],
                )->setPrices(
                    (object)[
                        'finalNetCost'   => 0.00,
                        'finalGrossCost' => 0.00,
                        'localizedPrice' => '0,00 EUR',
                    ],
                ),
            ]);
        $databaseRepository->method('getPaymentMethods')->willReturn(
            [$this->generatePaymentMethod(['shippingMethodID' => 2])],
        );
        $databaseRepository->method('getSurchargeForShippingMethod')->willReturnCallback(
            fn() => $this->generateShippingSurcharge(),
        );
        $databaseRepository->method('getPackagings')->willReturn(
            [
                $this->generatePackaging(),
                $this->generatePackaging(['kVerpackung' => 2]),
                $this->generatePackaging(['kVerpackung' => 3, 'fKostenfrei' => 500.85]),
            ]
        );
        $databaseRepository->method('getPaymentMethods')->willReturnCallback(
            function (...$args) {
                $data = (object)['shippingMethodID' => 1];
                return match ($args[0]) {
                    1       => [PaymentDTO::fromLegacyObject($data)],
                    default => [],
                };
            }
        );
        $cacheRepository = $this->createMock(CacheRepository::class);
        $languageHelper  = new LanguageHelperExtended();
        $cart            = $this->createMock(Cart::class);
        $cart->method('gibGesamtsummeWarenExt')->willReturnCallback(
            function (...$args) {
                // $gross parameter offset for gibGesamtsummeWarenExt()
                $grossParamOffset = 1;
                $returnValues     = [
                    'gross' => 1500.85,
                    'net'   => 1261.218487394958,
                ];

                return $returnValues[$args[$grossParamOffset] ? 'gross' : 'net'];
            }
        );

        $shippingService = new ShippingService(
            cart                 : $cart,
            customer             : Frontend::getCustomer(),
            currency             : Frontend::getCurrency(),
            config               : $config ?? [
                Checkout::SHIPPING_TAX_RATE->getValue() => Shopsetting::getInstance()->getString(
                    Checkout::SHIPPING_TAX_RATE->value,
                    \CONF_KAUFABWICKLUNG,
                ),
            ],
            database             : $databaseRepository,
            cache                : $cacheRepository,
        );
        $shippingService->setLanguageHelper($languageHelper);

        return $shippingService;
    }

    /**
     * This method is called before each test.
     */
    public function setUp(): void
    {
        parent::setUp();
        $_SESSION['Steuersatz'] = [19.00, 19.00, 7.00, 'abc'];
        $this->shippingService  = $this->getShippingService();
    }

    /**
     * This method is called after each test.
     */
    public function tearDown(): void
    {
        unset($_SESSION['possibleShippingMethods'], $_SESSION['Steuersatz']);
        parent::tearDown();
    }

    public function testGetLanguageHelper(): void
    {
        $this->expectNotToPerformAssertions();
        $this->shippingService->getLanguageHelper();
    }

    public function testSetLanguageHelper(): void
    {
        $this->expectNotToPerformAssertions();
        $this->shippingService->setLanguageHelper($this->shippingService->getLanguageHelper());
    }

    public function testGetCountryService(): void
    {
        $this->expectNotToPerformAssertions();
        $this->shippingService->getCountryService();
    }

    public function testSetCountryService(): void
    {
        $this->expectNotToPerformAssertions();
        $this->shippingService->setCountryService();
    }

    public function testGetDeliveryAddressFromUI(): void
    {
        $deliveryAddressBad         = [];
        $_POST['versandrechnerBTN'] = true;
        $_POST['land']              = 'DE';
        $_POST['plz']               = '10115';
        $deliveryAddressGood        = $this->shippingService->getDeliveryAddressFromUI();
        $_POST['plz']               = null;
        $deliveryAddressBad[]       = $this->shippingService->getDeliveryAddressFromUI();
        $_POST['plz']               = '10115';
        $_POST['land']              = 'GER';
        $deliveryAddressBad[]       = $this->shippingService->getDeliveryAddressFromUI();
        $_POST['land']              = 'DE';
        unset($_POST['versandrechnerBTN']);
        $deliveryAddressBad[] = $this->shippingService->getDeliveryAddressFromUI();

        $this->assertInstanceOf(Lieferadresse::class, $deliveryAddressGood);
        $this->assertTrue(
            $deliveryAddressGood->cLand === 'DE'
            && $deliveryAddressGood->cPLZ === '10115',
        );
        foreach ($deliveryAddressBad as $deliveryAddress) {
            $this->assertNull($deliveryAddress);
        }
    }

    public function testGetCustomerGroup(): void
    {
        $customerGroup = $this->shippingService->getCustomerGroup();
        $customerGroup->setID(1);
        $_SESSION['Kundengruppe'] = $customerGroup;
        $customerGroup            = $this->shippingService->getCustomerGroup();
        $this->assertEquals(1, $customerGroup->getID());
    }

    public function testGetAllShippingMethods(): void
    {
        $shippingMethods = $this->shippingService->getAllShippingMethods();
        $this->assertIsArray($shippingMethods);
    }

    public function testGetPossibleShippingMethods(): void
    {
        $possibleMethods = $this->shippingService->getPossibleShippingMethods();
        $this->assertIsArray($possibleMethods);
    }

    public function testSortMethodsByPrice(): void
    {
        $methods    = [
            0 => $this->generateShippingMethod(),
            1 => $this->generateShippingMethod(),
        ];
        $methods[0] = $methods[0]->setPrices(
            (object)[
                'finalNetCost'   => 1.00,
                'finalGrossCost' => 1.00,
                'localizedPrice' => '0,00 EUR',
            ],
        );

        $sortedMethods = $this->shippingService->sortMethodsByPrice($methods);
        $this->assertEquals('0,00 EUR', $sortedMethods[1]->localizedPrice);
    }

    public function testGetDeliveryText(): void
    {
        $this->assertEquals(
            'simpleglobal',
            $this->shippingService->getDeliveryText(2, 3, 'simple'),
        );
        $this->assertEquals(
            'abcglobal',
            $this->shippingService->getDeliveryText(2, 3, 'abc'),
        );
    }

    public function testGetPossibleFreeShippingMethods(): void
    {
        $deliveryCountryCode = 'DE';
        $deliveryZipCode     = '10115';
        $cartHash            = '93b20749cb4c97d843fbb825754b6300';
        $shippingDTO         = $this->generateShippingMethod();
        $shippingMethods     = [
            $shippingDTO,
            $shippingDTO,
            $this->generateShippingMethod(['freeShippingMinAmount' => 100.00]),
        ];
        $shippingMethods[1]  = $shippingMethods[1]->setPrices(
            (object)[
                'finalNetCost'   => 0.00,
                'finalGrossCost' => 0.00,
                'localizedPrice' => '0,00 EUR',
            ],
        );
        $shippingMethods[2]  = $shippingMethods[2]->setPrices(
            (object)[
                'finalNetCost'          => 1.00,
                'finalGrossCost'        => 1.00,
                'localizedPrice'        => '1,00 EUR',
            ],
        );

        $_SESSION['possibleShippingMethods'][$cartHash . $deliveryCountryCode . $deliveryZipCode] = $shippingMethods;
        $this->assertCount(
            2,
            $this->shippingService->getPossibleFreeShippingMethods($deliveryCountryCode, $deliveryZipCode),
        );
        $this->assertCount(
            1,
            $this->shippingService->getPossibleFreeShippingMethods(
                $deliveryCountryCode,
                $deliveryZipCode,
                50.00,
            ),
        );
        $this->assertCount(
            2,
            $this->shippingService->getPossibleFreeShippingMethods(
                $deliveryCountryCode,
                $deliveryZipCode,
                100.00,
            ),
        );
        unset($_SESSION['possibleShippingMethods'][$cartHash . $deliveryCountryCode . $deliveryZipCode]);
    }

    public function testGetFreeShippingCountriesByProduct(): void
    {
        $this->assertCount(0, $this->shippingService->getFreeShippingCountriesByProduct(
            (object)[
                'Preise' => (object)[
                    'fVK' => [
                        20.05,
                        16.85,
                    ],
                ],
                'kVersandklasse' => 1,
            ],
        ));
        $this->assertCount(2, $this->shippingService->getFreeShippingCountriesByProduct(
            (object)[
                'Preise' => (object)[
                    'fVK' => [
                        72.41,
                        60.85,
                    ],
                ],
                'kVersandklasse' => 1,
            ],
        ));
    }

    private function generateCartItems(): array
    {
        $cartItem1                    = new CartItem();
        $cartItem1->nPosTyp           = \C_WARENKORBPOS_TYP_ARTIKEL;
        $cartItem1->Artikel           = new Artikel();
        $cartItem1->nAnzahl           = 1.00;
        $cartItem1->kVersandklasse    = 3;
        $cartItem1->fPreisEinzelNetto = 8.85;
        $cartItem2                    = new CartItem();
        $cartItem2->nPosTyp           = \C_WARENKORBPOS_TYP_ARTIKEL;
        $cartItem2->Artikel           = new Artikel();
        $cartItem2->nAnzahl           = 1.00;
        $cartItem2->kVersandklasse    = 1;
        $cartItem2->fPreisEinzelNetto = 8.85;
        $cartItem3                    = new CartItem();
        $cartItem3->nPosTyp           = \C_WARENKORBPOS_TYP_ARTIKEL;
        $cartItem3->Artikel           = new Artikel();
        $cartItem3->nAnzahl           = 1.00;
        $cartItem3->kVersandklasse    = 1;
        $cartItem3->fPreisEinzelNetto = 8.85;
        $cartItem4                    = new CartItem();
        $cartItem4->nPosTyp           = \C_WARENKORBPOS_TYP_ARTIKEL;
        $cartItem4->Artikel           = new Artikel();
        $cartItem4->nAnzahl           = 1.00;
        $cartItem4->kVersandklasse    = 1;
        $cartItem4->fPreisEinzelNetto = 8.85;
        $cartItem5                    = new CartItem();
        $cartItem5->nPosTyp           = \C_WARENKORBPOS_TYP_ARTIKEL;
        $cartItem5->Artikel           = new Artikel();
        $cartItem5->nAnzahl           = 1.00;
        $cartItem5->kVersandklasse    = 2;
        $cartItem5->fPreisEinzelNetto = 8.85;

        $cartItem1->Artikel->kArtikel = 1;
        $cartItem2->Artikel->kArtikel = 2;
        $cartItem3->Artikel->kArtikel = 3;
        $cartItem4->Artikel->kArtikel = 5;
        $cartItem5->Artikel->kArtikel = 5;

        $cartItem1->Artikel->kSteuerklasse = 1;
        $cartItem2->Artikel->kSteuerklasse = 1;
        $cartItem3->Artikel->kSteuerklasse = 1;
        $cartItem4->Artikel->kSteuerklasse = 1;
        $cartItem5->Artikel->kSteuerklasse = 1;

        $cartItem2->Artikel->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN_GESTAFFELT] = 'DE 1-45,00:2-60,00:3-80;'
            . 'AT 1-90,00:2-120,00:3-150,00';
        $cartItem3->Artikel->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN]            = 'DE 600,00;AT 600,00';
        $cartItem4->Artikel->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN]            = 'DK 600,00;FR 600,00';
        $cartItem5->Artikel->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN]            = 'DK 600,00;FR 600,00';
        $cartItem5->Artikel->FunktionsAttribute[\FKT_ATTRIBUT_VERSANDKOSTEN_GESTAFFELT] = 'DE 1-45,00:2-60,00:3-80;'
            . 'AT 1-90,00:2-120,00:3-150,00';

        return [
            $cartItem1,
            $cartItem2,
            $cartItem3,
            $cartItem4,
            $cartItem5,
        ];
    }

    public function testCartIsDependent(): void
    {
        $cartItems = $this->generateCartItems();
        $this->assertFalse($this->shippingService->cartIsDependent());
        $this->assertFalse($this->shippingService->cartIsDependent('', [$cartItems[0]]));
        $this->assertTrue($this->shippingService->cartIsDependent('', [$cartItems[1], $cartItems[2]]));
        $this->assertFalse($this->shippingService->cartIsDependent('', [$cartItems[3]]));
        $this->assertTrue($this->shippingService->cartIsDependent('', [$cartItems[4]]));
        $this->assertFalse($this->shippingService->cartIsDependent('UK', [$cartItems[4]]));
    }

    public function testCartIsMixed(): void
    {
        $cartItems = $this->generateCartItems();
        $this->assertFalse($this->shippingService->cartIsMixed());
        $this->assertFalse($this->shippingService->cartIsMixed('', [$cartItems[0]]));
        $this->assertTrue($this->shippingService->cartIsMixed('', [$cartItems[1], $cartItems[2]]));
        $this->assertFalse($this->shippingService->cartIsMixed('', [$cartItems[3]]));
        $this->assertTrue($this->shippingService->cartIsMixed('', [$cartItems[4]]));
        $this->assertFalse($this->shippingService->cartIsMixed('UK', [$cartItems[4]]));
    }

    public function testFilterPaymentMethodByID(): void
    {
        $paymentMethod1 = $this->generatePaymentMethod([
            'id' => 1,
        ]);
        $paymentMethod2 = $this->generatePaymentMethod([
            'id' => 2,
        ]);

        $result1 = $this->shippingService->filterPaymentMethodByID(
            [$paymentMethod1, $paymentMethod2],
            1,
        );
        $result2 = $this->shippingService->filterPaymentMethodByID(
            [$paymentMethod1, $paymentMethod2],
            2,
        );
        $result3 = $this->shippingService->filterPaymentMethodByID(
            [$paymentMethod1, $paymentMethod2],
            0,
        );
        $this->assertNotNull($result1);
        $this->assertNotNull($result2);
        $this->assertNull($result3);
    }

    /**
     * @comment The filtering happens on SQL basis, so we can't test it here.
     */
    public function testGetPaymentMethodsByShipping(): void
    {
        $paymentMethods = $this->shippingService->getPaymentMethodsByShipping(2);
        $this->assertCount(1, $paymentMethods);
        $this->assertEquals(2, $paymentMethods[0]->shippingMethodID);
    }

    /**
     * @throws Exception
     */
    public function testGetVatNote(): void
    {
        $this->assertEquals('', $this->shippingService->getVatNote());
        $_SESSION['Kundengruppe'] = new CustomerGroup();
        $_SESSION['Kundengruppe']->setIsMerchant(1);
        $shippingService = $this->getShippingService();
        unset($_SESSION['Kundengruppe']);
        $this->assertEquals(' plusproductDetails vatproductDetails', $shippingService->getVatNote());
    }

    public function testFilterOutCheapestMethods(): void
    {
        $shippingMethods    = [
            $this->generateShippingMethod(['showAlways' => true, 'exclFromCheapestCalc' => false]),
            $this->generateShippingMethod(['showAlways' => false, 'exclFromCheapestCalc' => true]),
            $this->generateShippingMethod(['showAlways' => false, 'exclFromCheapestCalc' => false]),
            $this->generateShippingMethod(['showAlways' => false, 'exclFromCheapestCalc' => false]),
            $this->generateShippingMethod(['showAlways' => false, 'exclFromCheapestCalc' => false]),
            $this->generateShippingMethod(['showAlways' => true, 'exclFromCheapestCalc' => true]),
        ];
        $shippingMethods[0] = $shippingMethods[0]->setPrices(
            (object)[
                'finalNetCost'   => 999.00,
                'finalGrossCost' => 999.00,
                'localizedPrice' => '999,00 EUR',
            ],
        );
        $shippingMethods[1] = $shippingMethods[1]->setPrices(
            (object)[
                'finalNetCost'   => 0.00,
                'finalGrossCost' => 0.00,
                'localizedPrice' => '0,00 EUR',
            ],
        );
        $shippingMethods[2] = $shippingMethods[2]->setPrices(
            (object)[
                'finalNetCost'   => 10.00,
                'finalGrossCost' => 10.00,
                'localizedPrice' => '10,00 EUR',
            ],
        );
        $shippingMethods[3] = $shippingMethods[3]->setPrices(
            (object)[
                'finalNetCost'   => 5.00,
                'finalGrossCost' => 5.00,
                'localizedPrice' => '5,00 EUR',
            ],
        );
        $shippingMethods[4] = $shippingMethods[4]->setPrices(
            (object)[
                'finalNetCost'          => 1.00,
                'finalGrossCost'        => 1.00,
                'localizedPrice'        => '1,00 EUR',
            ],
        );
        $shippingMethods[5] = $shippingMethods[5]->setPrices(
            (object)[
                'finalNetCost'          => 2.00,
                'finalGrossCost'        => 2.00,
                'localizedPrice'        => '2,00 EUR',
            ],
        );

        $cheapestMethods = $this->shippingService->filterOutCheapestMethods($shippingMethods);
        $this->assertEquals(999.0, $cheapestMethods[0]->finalNetCost);
        $this->assertEquals(0.0, $cheapestMethods[1]->finalNetCost);
        $this->assertEquals(1.0, $cheapestMethods[2]->finalNetCost);
        $this->assertEquals(2.0, $cheapestMethods[3]->finalNetCost);
        $this->assertCount(4, $cheapestMethods);
    }

    public function testGetFavourableShippingForProduct(): void
    {
        $product                   = new Artikel();
        $product->Preise           = $this->createMock(Preise::class);
        $product->Preise->fVKNetto = 64.85;
        $product->kVersandklasse   = 3;
        $shippingService           = $this->getShippingService();
        $favourableShipping        = $shippingService->getFavourableShippingForProduct($product, 'DE');
        $this->assertEquals(2, $favourableShipping->id ?? 0);
    }

    public function testSimulateCustomShippingCosts(): void
    {
        $products                 = $this->generateCartItems();
        $dependentShippingMethods = [
            $this->generateShippingMethod(
                [
                    'id'                     => 1,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 50.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                ],
            ),
            $this->generateShippingMethod(
                [
                    'id'                     => 2,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 50.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                    'dependentShipping'      => true,
                ],
            ),
            $this->generateShippingMethod(
                [
                    'id'                     => 3,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 10.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                    'dependentShipping'      => true,
                ],
            ),
        ];

        $result1 = $this->shippingService->simulateCustomShippingCosts(
            'DE',
            $dependentShippingMethods,
            $products[2]->Artikel,
            1.00,
        );
        $result2 = $this->shippingService->simulateCustomShippingCosts(
            'DE',
            [$this->generateShippingMethod(
                [
                    'id'                     => 3,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 10.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                    'dependentShipping'      => true,
                    'includeTaxes'           => false,
                ],
            )],
            $products[2]->Artikel,
            1.00,
        );
        $result3 = $this->shippingService->simulateCustomShippingCosts(
            'DE',
            $dependentShippingMethods,
            $products[3]->Artikel,
            1.00,
        );
        $result4 = $this->shippingService->simulateCustomShippingCosts(
            'DE',
            $dependentShippingMethods,
            $products[1]->Artikel,
            1.00,
        );
        $this->assertEquals('600,00 &euro;', $result1->priceLocalized ?? '');
        $this->assertEquals('714,00 &euro;', $result2->priceLocalized ?? '');
        $this->assertNull($result3);
        $this->assertEquals('45,00 &euro;', $result4->priceLocalized ?? '');
    }

    public function testGetCustomShippingCostType(): void
    {
        $products = $this->generateCartItems();
        $result1  = $this->shippingService->getCustomShippingCostType($products[0]->Artikel);
        $result2  = $this->shippingService->getCustomShippingCostType($products[1]->Artikel);
        $result3  = $this->shippingService->getCustomShippingCostType($products[2]->Artikel);
        $this->assertEquals(ProductDependentShippingCosts::NONE->value, $result1->value);
        $this->assertEquals(ProductDependentShippingCosts::BULK->value, $result2->value);
        $this->assertEquals(ProductDependentShippingCosts::FIX->value, $result3->value);
    }

    public function testGetCustomShippingCostsByProduct(): void
    {
        $products        = $this->generateCartItems();
        $shippingMethods = [
            $this->generateShippingMethod(
                [
                    'id'                     => 1,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 50.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                ],
            ),
            $this->generateShippingMethod(
                [
                    'id'                     => 2,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 50.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                    'dependentShipping'      => true,
                ],
            ),
            $this->generateShippingMethod(
                [
                    'id'                     => 3,
                    'price'                  => 8.85,
                    'freeShippingMinAmount'  => 50.00,
                    'allowedShippingClasses' => ['-1'],
                    'countries'              => [
                        'DE',
                        'AT',
                    ],
                    'dependentShipping'      => true,
                    'includeTaxes'           => false,
                ],
            ),
        ];

        $result1 = $this->shippingService->getCustomShippingCostsByProduct(
            'DE',
            $products[2]->Artikel,
            1.00,
            $shippingMethods[0],
        );
        $result2 = $this->shippingService->getCustomShippingCostsByProduct(
            'DE',
            $products[2]->Artikel,
            1.00,
            $shippingMethods[1],
        );
        $result3 = $this->shippingService->getCustomShippingCostsByProduct(
            'DE',
            $products[2]->Artikel,
            1.00,
            $shippingMethods[2],
        );
        $result4 = $this->shippingService->getCustomShippingCostsByProduct(
            'FR',
            $products[2]->Artikel,
            1.00,
            $shippingMethods[2],
        );
        $this->assertNull($result1);
        $this->assertEquals('600,00 &euro;', $result2->priceLocalized ?? '');
        $this->assertEquals('714,00 &euro;', $result3->priceLocalized ?? '');
        $this->assertNull($result4);
    }

    public function testGetCustomShippingCostsByCart(): void
    {
        $products = $this->generateCartItems();
        $result   = $this->shippingService->getCustomShippingCostsByCart('DE', $products);
        $this->assertCount(2, $result);
        $this->assertEquals(45.00, $result[0]->netPrice ?? 0.00);
        $this->assertEquals(600.00, $result[1]->netPrice ?? 0.00);
    }

    public function testGetTaxRate(): void
    {
        $result1 = $this->shippingService->getTaxRate(1);
        $result2 = $this->shippingService->getTaxRate(2);
        $this->assertEquals(19.00, $result1);
        $this->assertEquals(7.00, $result2);
        $this->expectException(\RuntimeException::class);
        $this->shippingService->getTaxRate();
        $this->expectException(\RuntimeException::class);
        $this->shippingService->getTaxRate(2);
    }

    public function testGetShippingClasses(): void
    {
        $products = $this->generateCartItems();
        $result   = $this->shippingService->getShippingClasses($products);
        $this->assertTrue(
            $result[0] === 1
            && $result[1] === 2
            && $result[2] === 3
        );
    }

    public function testGetShippingSurcharge(): void
    {
        $method1 = $this->generateShippingMethod();
        $method2 = $this->generateShippingMethod(['includeTaxes' => false]);
        $result1 = $this->shippingService->getShippingSurcharge(
            $method1,
            'DE',
            '10115'
        );
        $result2 = $this->shippingService->getShippingSurcharge(
            $method2,
            'DE',
            '10115'
        );
        $this->assertEquals('3,85 &euro;', $result1->surchargeLocalized ?? '');
        $this->assertEquals('4,58 &euro;', $result2->surchargeLocalized ?? '');

        $_SESSION['Kundengruppe'] = new CustomerGroup();
        $_SESSION['Kundengruppe']->setIsMerchant(1);
        $shippingService = $this->getShippingService();

        $result1 = $shippingService->getShippingSurcharge(
            $method1,
            'DE',
            '10115'
        );
        $result2 = $shippingService->getShippingSurcharge(
            $method2,
            'DE',
            '10115'
        );
        $this->assertEquals('3,24 &euro;', $result1->surchargeLocalized ?? '');
        $this->assertEquals('3,85 &euro;', $result2->surchargeLocalized ?? '');
        unset($_SESSION['Kundengruppe']);
    }

    public function testCalcCostForShippingMethod(): void
    {
        $shippingMethod          = $this->generateShippingMethod(['price' => 8.85]);
        $dependentShippingMethod = $this->generateShippingMethod(['price' => 8.85, 'dependentShipping' => true]);
        $shippingMethodMaxPrice  = $this->generateShippingMethod(['price' => 8.85, 'maxPrice' => 1.85]);
        $shippingMethodFree      = $this->generateShippingMethod(['price' => 8.85, 'freeShippingMinAmount' => 50.00]);
        $products                = $this->generateCartItems();
        $productFreeShipping     = $products[0]->Artikel;

        $productFreeShipping->Preise           = $this->createMock(Preise::class);
        $productFreeShipping->Preise->fVKNetto = 90.85;
        $productFreeShipping->Preise->fVK[0]   = 76.34453781512605;
        $productFreeShipping->Preise->fVK[1]   = 90.85;

        $result1 = $this->shippingService->calcCostForShippingMethod(
            $shippingMethod,
            'DE',
            $products[0]->Artikel,
        ) - 7;
        $result2 = $this->shippingService->calcCostForShippingMethod(
            $dependentShippingMethod,
            'DE',
            $products[2]->Artikel,
        ) - 511;
        $result3 = $this->shippingService->calcCostForShippingMethod(
            $shippingMethodMaxPrice,
            'DE',
            $products[0]->Artikel,
        ) - 1;
        $result4 = $this->shippingService->calcCostForShippingMethod(
            $shippingMethodFree,
            'DE',
            $productFreeShipping,
        );
        $this->assertTrue($result1 > 0 && $result1 < 1);
        $this->assertTrue($result2 > 0 && $result2 < 1);
        $this->assertTrue($result3 > 0 && $result3 < 1);
        $this->assertEquals(0.00, $result4);
    }

    public function testGetProductPrice(): void
    {
        $shippingMethod1       = $this->generateShippingMethod(['price' => 8.85]);
        $shippingMethod2       = $this->generateShippingMethod(['price' => 8.85, 'includeTaxes' => false]);
        [$product1, $product2] = \array_map(
            static fn($item) => $item->Artikel,
            $this->generateCartItems()
        );

        $product1->Preise           = $this->createMock(Preise::class);
        $product1->Preise->fVKNetto = 90.85;
        $product1->Preise->fVK[0]   = 76.34453781512605;
        $product1->Preise->fVK[1]   = 90.85;

        $product2->Preise           = $this->createMock(Preise::class);
        $product2->Preise->fVKNetto = 8.85;
        $product2->Preise->fVK[0]   = 7.436974789915966;
        $product2->Preise->fVK[1]   = 8.85;

        $result1 = $this->shippingService->getProductPrice($product1, $shippingMethod1);
        $result2 = $this->shippingService->getProductPrice($product2, $shippingMethod2);

        $this->assertEquals(90.85, $result1);
        $this->assertTrue($result2 > 7.4 && $result2 < 7.5);
    }

    public function testGetLowestShippingFeesForProduct(): void
    {
        [$product1, $product2] = \array_map(
            static fn($item) => $item->Artikel,
            $this->generateCartItems()
        );

        $product1->Preise           = $this->createMock(Preise::class);
        $product1->Preise->fVKNetto = 90.85;
        $product1->Preise->fVK[0]   = 76.34453781512605;
        $product1->Preise->fVK[1]   = 90.85;

        $product2->Preise           = $this->createMock(Preise::class);
        $product2->Preise->fVKNetto = 8.85;
        $product2->Preise->fVK[0]   = 7.436974789915966;
        $product2->Preise->fVK[1]   = 8.85;

        $result1 = $this->shippingService->getLowestShippingFeesForProduct('DE', $product1, false);
        $result2 = $this->shippingService->getLowestShippingFeesForProduct('DE', $product2, true);
        $this->assertEquals(8.85, $result1);
        $this->assertTrue($result2 > 6.50 && $result2 < 6.60);
    }

    public function testGetDeliverytimeEstimationText(): void
    {
        $result1 = $this->shippingService->getDeliverytimeEstimationText(2, 3);
        $result2 = $this->shippingService->getDeliverytimeEstimationText(8, 8);
        $result3 = $this->shippingService->getDeliverytimeEstimationText(16, 24);
        $result4 = $this->shippingService->getDeliverytimeEstimationText(32, 32);
        $result5 = $this->shippingService->getDeliverytimeEstimationText(64, 96);
        $result6 = $this->shippingService->getDeliverytimeEstimationText(128, 128);

        $this->assertEquals('deliverytimeEstimationglobal', $result1);
        $this->assertEquals('deliverytimeEstimationSimpleglobal', $result2);
        $this->assertEquals('deliverytimeEstimationWeeksglobal', $result3);
        $this->assertEquals('deliverytimeEstimationSimpleWeeksglobal', $result4);
        $this->assertEquals('deliverytimeEstimationMonthsglobal', $result5);
        $this->assertEquals('deliverytimeEstimationSimpleMonthsglobal', $result6);
    }

    public function testGetShippingFreeString(): void
    {
        $shippingMethod1 = $this->generateShippingMethod(['price' => 8.85, 'freeShippingMinAmount' => 2000.85]);
        $shippingMethod2 = $this->generateShippingMethod(['price' => 8.85, 'freeShippingMinAmount' => 500.85]);
        $result1         = $this->shippingService->getShippingFreeString($shippingMethod1);
        $result2         = $this->shippingService->getShippingFreeString($shippingMethod2);

        $this->assertEquals('noShippingCostsAtbasket', $result1);
        $this->assertEquals('noShippingCostsReachedbasket', $result2);
    }

    public function testGetNetShippingFreeDifference(): void
    {
        $shippingMethod1 = $this->generateShippingMethod(['price' => 8.85, 'freeShippingMinAmount' => 2000.85]);
        $shippingMethod2 = $this->generateShippingMethod([
            'price' => 8.85,
            'includeTaxes' => false,
            'freeShippingMinAmount' => 2000.85
        ]);
        $result1         = $this->shippingService->getNetShippingFreeDifference($shippingMethod1);
        $result2         = $this->shippingService->getNetShippingFreeDifference($shippingMethod2);

        $this->assertTrue($result1 > 420.00 && $result1 < 421.00);
        $this->assertTrue($result2 > 739.00 && $result2 < 740.00);
    }

    public function testGetShippingFreeCountriesString(): void
    {
        $availableLanguages = $this->shippingService->getLanguageHelper()->getAvailable();
        $germany            = (new Country(
            'DE',
            false,
            $availableLanguages,
        ))->setNames([1 => 'Deutschland']);
        $austria            = (new Country(
            'AT',
            false,
            $availableLanguages,
        ))->setNames([1 => 'Österreich']);
        $countryService     = $this->shippingService->getCountryService();
        $countryService->addCountryToList($germany);
        $countryService->addCountryToList($austria);
        $this->shippingService->setCountryService($countryService);

        $shippingMethod1 = $this->generateShippingMethod();
        $shippingMethod2 = $this->generateShippingMethod([
            'freeShippingMinAmount' => 8.85,
            'countries' => ['DE', 'AT'],
        ]);
        $result1         = $this->shippingService->getShippingFreeCountriesString($shippingMethod1);
        $result2         = $this->shippingService->getShippingFreeCountriesString($shippingMethod2);

        $this->assertEquals('', $result1);
        $this->assertEquals('Deutschland, Österreich', $result2);
    }

    public function testGetFreeShippingMethod(): void
    {
        $this->assertEquals(8, $this->shippingService->getFreeShippingMethod()->id ?? 0);
    }

    public function testGetPossibleShippingCountries(): void
    {
        $this->assertEquals([], $this->shippingService->getPossibleShippingCountries());
    }

    public function testGetPossiblePackagings(): void
    {
        $result = $this->shippingService->getPossiblePackagings();
        $this->assertEquals(0, $result[0]->nKostenfrei);
        $this->assertEquals('5,85 &euro;', $result[1]->fBruttoLocalized);
        $this->assertEquals(1, $result[2]->nKostenfrei);
    }

    public function testGetShippingByPaymentMethodID(): void
    {
        $shippingMethod = $this->generateShippingMethod(['id' => 1]);
        $result1        = $this->shippingService->getShippingByPaymentMethodID(
            [$shippingMethod],
            1
        );
        $result2        = $this->shippingService->getShippingByPaymentMethodID(
            [$shippingMethod],
            2
        );

        $this->assertEquals(1, $result1->id ?? 0);
        $this->assertNull($result2);
    }

    public function testGetShippingMethodByID(): void
    {
        $this->assertEquals(0, $this->shippingService->getPaymentMethodID());
        $_SESSION['Zahlungsart'] = (object)['kZahlungsart' => 1];
        $this->assertEquals(1, $this->shippingService->getPaymentMethodID());
        unset($_SESSION['Zahlungsart']);
    }

    public function testCalculateNetPrice(): void
    {
        $result1 = $this->shippingService->calculateNetPrice(8.85, 1);
        $result2 = $this->shippingService->calculateNetPrice(8.85, 2);
        $this->assertTrue($result1 > 7.40 && $result1 < 7.50);
        $this->assertTrue($result2 > 8.20 && $result2 < 8.30);
    }

    public function testCalculateGrossPrice(): void
    {
        $result1 = $this->shippingService->calculateGrossPrice(8.85, 1);
        $result2 = $this->shippingService->calculateGrossPrice(8.85, 2);
        $this->assertEquals(10.53, $result1);
        $this->assertEquals(9.47, $result2);
    }

    public function testGetDependentShippingMethod(): void
    {
        $cartItemsWithCustomCosts = \array_filter(
            $this->generateCartItems(),
            static fn($item) => $item->Artikel->isUsedForShippingCostCalculation('DE') === false,
        );

        $result1 = $this->shippingService->getDependentShippingMethod('DE', $cartItemsWithCustomCosts);
        $result2 = $this->shippingService->getDependentShippingMethod('FR', $cartItemsWithCustomCosts);

        $this->assertEquals(4, $result1->id ?? 0);
        $this->assertNull($result2);
    }

    public function testGetTaxRateIDs(): void
    {
        $cartItems         = $this->generateCartItems();
        $shippingServiceUS = $this->getShippingService([
            Checkout::SHIPPING_TAX_RATE->getValue() => 'US',
        ]);
        $shippingServiceHS = $this->getShippingService([
            Checkout::SHIPPING_TAX_RATE->getValue() => 'HS',
        ]);
        $shippingServicePS = $this->getShippingService([
            Checkout::SHIPPING_TAX_RATE->getValue() => 'PS',
        ]);
        $shippingServiceAA = $this->getShippingService([
            Checkout::SHIPPING_TAX_RATE->getValue() => 'AA',
        ]);

        $cartItems[0]->kSteuerklasse = 2;
        $cartItems[1]->kSteuerklasse = 2;
        $cartItems[2]->kSteuerklasse = 2;
        $cartItems[3]->kSteuerklasse = 1;
        $cartItems[4]->kSteuerklasse = 1;

        $result1 = $shippingServiceUS->getTaxRateIDs($cartItems);

        $cartItems[0]->kSteuerklasse = 1;
        $cartItems[1]->kSteuerklasse = 1;
        $cartItems[2]->kSteuerklasse = 1;
        $cartItems[3]->kSteuerklasse = 2;
        $cartItems[4]->kSteuerklasse = 2;

        $result2 = $shippingServiceHS->getTaxRateIDs($cartItems);
        $result3 = $shippingServicePS->getTaxRateIDs($cartItems);
        $result4 = $shippingServiceAA->getTaxRateIDs($cartItems);

        $this->assertEquals(2, $result1[0]->taxRateID ?? 0);
        $this->assertEquals(1, $result2[0]->taxRateID ?? 0);
        $this->assertEquals(60.00, $result3[0]->proportion ?? 0);
        $this->assertEquals(40.00, $result3[1]->proportion ?? 0);
        $this->assertEquals([], $result4);
    }

    public function testGetFirstShippingMethod(): void
    {
        $shippingMethod1 = $this->generateShippingMethod(['id' => 1]);
        $shippingMethod2 = $this->generateShippingMethod(['id' => 2]);
        $result1         = $this->shippingService->getFirstShippingMethod(
            [$shippingMethod1, $shippingMethod2],
            1
        );
        $result2         = $this->shippingService->getFirstShippingMethod(
            [$shippingMethod2, $shippingMethod1],
            1
        );
        $result3         = $this->shippingService->getFirstShippingMethod(
            [$shippingMethod1, $shippingMethod2],
            2
        );

        $this->assertEquals(1, $result1->id ?? 0);
        $this->assertEquals(2, $result2->id ?? 0);
        $this->assertNull($result3);
    }

    public function testGetSortedPossibleShippings(): void
    {
        $sortedMethods     = $this->shippingService->getSortedPossibleShippings();
        $lastShippingPrice = 0;
        foreach ($sortedMethods as $method) {
            $this->assertGreaterThanOrEqual($lastShippingPrice, $method->finalNetCost);
            $lastShippingPrice = $method->finalNetCost;
        }
    }
}
