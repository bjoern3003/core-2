<?php

declare(strict_types=1);

namespace Tests\Helpers;

use JTL\Helpers\Text;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class TextTest
 * @package Tests\Helpers
 */
final class TextTest extends TestCase
{
    public function testStartsWith(): void
    {
        $this->assertTrue(Text::startsWith('123abc', '123'));
        $this->assertTrue(Text::startsWith('abc', ''));
        $this->assertFalse(Text::startsWith('123abc', 'q123'));
    }

    public function testEndsWith(): void
    {
        $this->assertTrue(Text::endsWith('123abc', 'abc'));
        $this->assertTrue(Text::endsWith('123abc', 'c'));
        $this->assertTrue(Text::endsWith('abc', ''));
        $this->assertFalse(Text::endsWith('123abc', 'q123'));
        $this->assertFalse(Text::endsWith('', '1'));
    }

    public function testHtmlEntities(): void
    {
        $this->assertSame('&euro;', Text::htmlentities('€'));
        $this->assertSame('&lt;div&gt;&lt;p&gt;s&lt;/p&gt;', Text::htmlentities('<div><p>s</p>'));
    }

    public function testHtmlEntitiesOnce(): void
    {
        $this->assertSame('&euro;', Text::htmlentitiesOnce('€'));
        $this->assertSame('&lt;div&gt;&lt;p&gt;s&lt;/p&gt;', Text::htmlentitiesOnce('<div><p>s</p>'));
    }

    public function testHtmlEntitiesSubstring(): void
    {
        $this->assertSame('&lt;p', Text::htmlentitiesSubstr('&lt;p&gt;test&lt;/p&gt;', 2));
    }

    public function testUnhtmlentities(): void
    {
        $this->assertNull(Text::unhtmlentities(null));
        $this->assertSame(42, Text::unhtmlentities(42));
        $this->assertSame('<p>test</p>', Text::unhtmlentities('&lt;p&gt;test&lt;/p&gt;'));
        $input = '<p>test\'s "are" good</p>';
        $this->assertSame($input, Text::unhtmlentities(Text::htmlentitydecode($input)));
    }

    public function testHtmlentitydecode(): void
    {
        $this->assertSame('&', Text::htmlentitydecode('&amp;'));
        $this->assertSame('&', Text::htmlentitydecode('&'));
    }

    public function testHtmlspecialchars(): void
    {
        $this->assertSame('&amp;', Text::htmlspecialchars('&'));
        $this->assertSame('&quot;', Text::htmlspecialchars('"'));
        $this->assertSame('"', Text::htmlspecialchars('"', \ENT_NOQUOTES));
    }

    public function testGethtmltranslationtable(): void
    {
        $this->assertIsArray(Text::gethtmltranslationtable());
    }

    public function testFilterXSS(): void
    {
        $this->assertSame('test', Text::filterXSS('test'));
        $this->assertSame(['test', 'test'], Text::filterXSS(['test', 'test']));
        $this->assertSame('123', Text::filterXSS(123));
        $this->assertSame('alert(1)', Text::filterXSS('<script>alert(1)</script>'));
        $this->assertSame(
            ['alert(1)', 'alert(2)'],
            Text::filterXSS(['<script>alert(1)</script>', '<script>alert(2)</script>'])
        );
        $this->assertSame('alert(1)', Text::filterXSS('<script type="text/javascript">alert(1)</script>'));
        $this->assertSame('alert(1)', Text::filterXSS('<script type="javascript">alert(1)</script>'));
    }

    public function testIsUtf8(): void
    {
        $this->assertSame(1, Text::is_utf8('test'));
        $this->assertSame(1, Text::is_utf8(\mb_convert_encoding('test', 'UTF-8', 'ISO-8859-1')));
        $this->assertSame(1, Text::is_utf8('täßt'));
        $this->assertSame(0, Text::is_utf8(\mb_convert_encoding('täst', 'ISO-8859-1', 'UTF-8')));
    }

    public function testXssClean(): void
    {
        $this->assertSame('test', Text::xssClean('test'));
        $this->assertSame('täst', Text::xssClean('t&auml;st'));
        $this->assertSame('alert(1)', Text::xssClean('<script>alert(1)</script>'));
    }

    public function testConvertUTF8(): void
    {
        $this->assertSame('test', Text::convertUTF8('test'));
        $this->assertSame('täst', Text::convertUTF8(\mb_convert_encoding('täst', 'ISO-8859-1', 'UTF-8')));
    }

    public function testConvertISO(): void
    {
        $this->assertSame('test', Text::convertISO('test'));
        $this->assertSame(\mb_convert_encoding('täst', 'ISO-8859-1', 'UTF-8'), Text::convertISO('täst'));
    }

    public function testConvertISO2ISO639(): void
    {
        $this->assertSame('test', Text::convertISO2ISO639('test'));
        $this->assertSame('ba', Text::convertISO2ISO639('bak'));
        $this->assertSame('de', Text::convertISO2ISO639('ger'));
        $this->assertSame('de', Text::convertISO2ISO639('de'));
    }

    public function testConvertISO6392ISO(): void
    {
        $this->assertSame('ger', Text::convertISO6392ISO('de'));
        $this->assertSame('ger', Text::convertISO6392ISO('ger'));
        $this->assertSame('bak', Text::convertISO6392ISO('ba'));
        $this->assertSame('ba', Text::convertISO2ISO639(Text::convertISO6392ISO('ba')));
    }

    public function testGetISOMappings(): void
    {
        $this->assertCount(184, Text::getISOMappings());
    }

    public function testRemoveDoubleSpaces(): void
    {
        $this->assertSame('test 123 456', Text::removeDoubleSpaces('test    123  456'));
    }

    public function testRemoveWhitespace(): void
    {
        $this->assertSame('test 123 456', Text::removeWhitespace('test    123  456'));
    }

    public function testCreateSSK(): void
    {
        $this->assertSame('', Text::createSSK('test123456'));
        $this->assertSame('', Text::createSSK(123));
        $this->assertSame('', Text::createSSK([]));
        $this->assertSame(';a;', Text::createSSK(['a']));
        $this->assertSame(';1;2;a;b;', Text::createSSK([1, 2, 'a', 'b']));
    }

    public function testParseSSK(): void
    {
        $this->assertSame([], Text::parseSSK(123));
        $this->assertSame([], Text::parseSSK(123));
        $this->assertSame([], Text::parseSSK([]));
        $this->assertSame([0 => 'a', 1 => 'b'], Text::parseSSK('a;b'));
        $this->assertSame([1 => '1', 2 => '2', 3 => 'a', 4 => 'b'], Text::parseSSK(';1;2;a;b;'));
        $this->assertSame([1 => 'a'], Text::parseSSK(Text::createSSK(['a'])));
    }

    public function testParseSSKint(): void
    {
        $this->assertSame([0 => 1, 1 => 2], Text::parseSSKint('1;2;'));
        $this->assertSame([1 => 1, 2 => 2], Text::parseSSKint(';1;2;'));
        $this->assertSame([], Text::parseSSKint([]));
        $this->assertSame([1 => '1', 2 => '2', 3 => 'a', 4 => 'b'], Text::parseSSK(';1;2;a;b;'));
        $this->assertSame([0 => 0, 1 => 0, 2 => 1], Text::parseSSKint('a;b;1;'));
        $this->assertSame([], Text::parseSSKint([]));
        $this->assertSame([], Text::parseSSKint(123));
    }

    /**
     * @param mixed $in Input
     * @param mixed $expect Expected output
     * @param bool  $validate
     * @param bool  $addSchema
     */
    #[DataProvider('providesFilterURL')]
    public function testFilterURL(mixed $in, mixed $expect, bool $validate = true, bool $addSchema = false): void
    {
        $this->assertSame($expect, Text::filterURL($in, $validate, $addSchema));
    }

    /**
     * @return string[][]
     */
    public static function providesFilterURL(): array
    {
        return [
            'hashtag'               => ['#', false],
            'queryparam'            => ['?foo=bar&baz=42', false],
            'array'                 => [[], false],
            'array v'               => [[], false, false],
            'int'                   => [11, false],
            'int v'                 => [11, false, false],
            'obj'                   => [(object)[], false],
            'obj v'                 => [(object)[], false, false],
            'no tld'                => ['example', false],
            'no tld v'              => ['example', 'example', false],
            'no proto v'            => ['example.com', false],
            'no proto'              => ['example.com', 'example.com', false],
            'no proto add v'        => ['example.com', 'http://example.com', true, true],
            'no proto add'          => ['example.com', 'http://example.com', false, true],
            'normal v'              => ['http://example.com', 'http://example.com'],
            'normal'                => ['http://example.com', 'http://example.com', false],
            'normal brackets v'     => ['(http://example.com)', false],
            'normal brackets'       => ['(http://example.com)', '(http://example.com)', false],
            'https v'               => ['https://example.com', 'https://example.com'],
            'https'                 => ['https://example.com', 'https://example.com', false],
            'umlaut v'              => ['https://ümläüt.com', 'https://xn--mlt-rla1jd.com'],
            'umlaut'                => ['https://ümläüt.com', 'https://xn--mlt-rla1jd.com', false],
            'umlaut no proto v'     => ['ümläüt.com', false, true],
            'umlaut no proto'       => ['ümläüt.com', 'xn--mlt-rla1jd.com', false],
            'umlaut no proto add v' => ['ümläüt.com', 'http://xn--mlt-rla1jd.com', true, true],
            'umlaut no proto add'   => ['ümläüt.com', 'http://xn--mlt-rla1jd.com', false, true],
        ];
    }

    /**
     * @param mixed $in Input
     * @param mixed $expect Expected output
     * @param bool  $validate
     */
    #[DataProvider('providesFilterEmailAddress')]
    public function testFilterEmailAddress(mixed $in, mixed $expect, bool $validate = true): void
    {
        $this->assertSame($expect, Text::filterEmailAddress($in, $validate));
    }

    /**
     * @return string[][]
     */
    public static function providesFilterEmailAddress(): array
    {
        return [
            'array'                             => [[], false],
            'array v'                           => [[], '', false],
            'int'                               => [11, false],
            'int v'                             => [11, '', false],
            'obj'                               => [(object)[], false],
            'obj v'                             => [(object)[], '', false],
            'no tld'                            => ['user@example', false],
            'no tld v'                          => ['user@example', 'user@example', false],
            'no tld brackets'                   => ['(user@example)', false],
            'no tld brackets v'                 => ['(user@example)', 'user@example', false],
            'no domain'                         => ['user', false],
            'no domain v'                       => ['user', false, false],
            'no domain no at'                   => ['user@', false],
            'no domain no at v'                 => ['user@', 'user@', false],
            'no domain umlaut'                  => ['(info@ümlüt)', false],
            'no domain umlaut v'                => ['(info@ümlüt)', false, true],
            'no domain no at umlaut'            => ['infoümlüt', false],
            'no domain no at umlaut v'          => ['infoümlüt', false, true],
            'no domain no at umlaut brackets'   => ['(infoümlüt)', false],
            'no domain no at umlaut brackets v' => ['(infoümlüt)', false, false],
            'multi at'                          => ['user@example@com', false],
            'multi at v'                        => ['user@example@com', false, false],
            'normal'                            => ['user@example.com', 'user@example.com'],
            'normal v'                          => ['user@example.com', 'user@example.com', false],
            'brackets'                          => ['(user@example.com)', 'user@example.com'],
            'brackets2'                         => ['(user@example.com)', 'user@example.com', false],
            'umlauts'                           => ['info@ümlüt.de', 'info@xn--mlt-goac.de'],
            'umlauts non-utf8'                  => [
                \mb_convert_encoding('info@ümlüt.de', 'ISO-8859-1', 'UTF-8'),
                'info@xn--mlt-goac.de'
            ],
            'umlauts v'                         => ['info@ümlüt.de', 'info@xn--mlt-goac.de', false],
            'umlauts non-utf8 v'                => [
                \mb_convert_encoding('info@ümlüt.de', 'ISO-8859-1', 'UTF-8'),
                'info@xn--mlt-goac.de',
                false
            ],
            'umlauts brackets'                  => ['(info@ümlüt.de)', 'info@xn--mlt-goac.de'],
            'umlauts brackets v'                => ['(info@ümlüt.de)', 'info@xn--mlt-goac.de', false],
        ];
    }

    public function testBuildUrl(): void
    {
        $arr = [];
        $this->assertSame('', Text::buildUrl($arr));
        $arr['scheme'] = 'https';
        $this->assertSame('https://', Text::buildUrl($arr));
        $arr['user'] = 'somebody';
        $arr['pass'] = 'secret';
        $this->assertSame('https://somebody:secret@', Text::buildUrl($arr));
        $arr['host'] = 'example.com';
        $this->assertSame('https://somebody:secret@example.com', Text::buildUrl($arr));
        $arr['port'] = 12345;
        $this->assertSame('https://somebody:secret@example.com:12345', Text::buildUrl($arr));
        $arr['path'] = '/foo';
        $this->assertSame('https://somebody:secret@example.com:12345/foo', Text::buildUrl($arr));
        $arr['query'] = 'a=1';
        $this->assertSame('https://somebody:secret@example.com:12345/foo?a=1', Text::buildUrl($arr));
        $arr['fragment'] = 'bar';
        $this->assertSame('https://somebody:secret@example.com:12345/foo?a=1#bar', Text::buildUrl($arr));
        $arr['foobar'] = 'baz';
        $this->assertSame('https://somebody:secret@example.com:12345/foo?a=1#bar', Text::buildUrl($arr));
    }

    public function testCheckPhoneNumber(): void
    {
        $this->assertSame(1, Text::checkPhoneNumber(''));
        $this->assertSame(0, Text::checkPhoneNumber('', false));
        $this->assertSame(0, Text::checkPhoneNumber('123456'));
        $this->assertSame(0, Text::checkPhoneNumber('030-123456'));
        $this->assertSame(0, Text::checkPhoneNumber('030/123456'));
        $this->assertSame(0, Text::checkPhoneNumber('+49 030/123456'));
        $this->assertSame(0, Text::checkPhoneNumber('+49 030 123456'));
        $this->assertSame(0, Text::checkPhoneNumber('+49 030 - 123456'));
        $this->assertSame(2, Text::checkPhoneNumber('030a123456'));
        $this->assertSame(2, Text::checkPhoneNumber('a123456'));
        $this->assertSame(2, Text::checkPhoneNumber('a123456'));
    }

    public function testCheckDate(): void
    {
        $this->assertSame(2, Text::checkDate('1234'));
        $this->assertSame(2, Text::checkDate('1234', false));
        $this->assertSame(1, Text::checkDate(''));
        $this->assertSame(0, Text::checkDate('', false));
        $this->assertSame(2, Text::checkDate('2021-09-01'));
        $this->assertSame(2, Text::checkDate('2021-09-01', false));
        $this->assertSame(0, Text::checkDate('01.09.2021'));
        $this->assertSame(0, Text::checkDate('01.09.2021', false));
        $this->assertSame(3, Text::checkDate('13.14.2021'));
        $this->assertSame(3, Text::checkDate('13.14.2021', false));
        $this->assertSame(0, Text::checkDate('01.01.0001'));
        $this->assertSame(0, Text::checkDate('01.01.0001', false));
        $this->assertSame(0, Text::checkDate('1.9.1984'));
        $this->assertSame(0, Text::checkDate('1.9.1984', false));
        $this->assertSame(2, Text::checkDate('1.9.84'));
        $this->assertSame(2, Text::checkDate('1.9.84', false));
        $this->assertSame(2, Text::checkDate('May 01 1970'));
        $this->assertSame(2, Text::checkDate('May 01 1970', false));
    }

    public function testFormatSize(): void
    {
        $size = 1024;
        $this->assertSame('1024.00 b', Text::formatSize($size));
        $this->assertSame('1024 b', Text::formatSize($size, '%.0f'));
        $this->assertSame('1024.0000 b', Text::formatSize($size, '%.4f'));
        $size *= 1024;
        $this->assertSame('1024.00 Kb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Mb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Gb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Tb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Pb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Eb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Zb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1024.00 Yb', Text::formatSize($size));
        $size *= 1024;
        $this->assertSame('1048576.00 Yb', Text::formatSize($size));
    }

    public function testUtf8ConvertRecursive(): void
    {
        $arr = ['täst', 'foo' => ['foobar', 44], 1];
        $this->assertSame($arr, Text::utf8_convert_recursive($arr));
        $utf8arr = [\mb_convert_encoding('täst', 'ISO-8859-1', 'UTF-8'), 'foo' => ['foobar', 44], 1];
        $this->assertSame($arr, Text::utf8_convert_recursive($utf8arr));
    }

    public function testJsonSafeEncode(): void
    {
        $json = '{"0":"t\u00e4st","foo":["foobar",44],"1":1}';
        $arr  = ['täst', 'foo' => ['foobar', 44], 1];
        $this->assertSame($json, Text::json_safe_encode($arr));
        $utf8arr = [\mb_convert_encoding('täst', 'ISO-8859-1', 'UTF-8'), 'foo' => ['foobar', 44], 1];
        $this->assertSame($json, Text::json_safe_encode($utf8arr));
    }

    public function testRemoveNumerousWhitespaces(): void
    {
        $expected = ' 123 456 ';
        $this->assertSame($expected, Text::removeNumerousWhitespaces($expected . '      '));
        $this->assertSame($expected, Text::removeNumerousWhitespaces('     ' . $expected . '      '));
    }

    public function testReplaceUmlauts(): void
    {
        $this->assertSame('test', Text::replaceUmlauts('test'));
        $this->assertSame('taest', Text::replaceUmlauts('täst'));
        $this->assertSame('tAest', Text::replaceUmlauts('tÄst'));
        $this->assertSame('oelig', Text::replaceUmlauts('ölig'));
        $this->assertSame('Oel', Text::replaceUmlauts('Öl'));
        $this->assertSame('suess', Text::replaceUmlauts('süß'));
        $this->assertSame('Uebel', Text::replaceUmlauts('Übel'));
        $this->assertSame('taest', Text::replaceUmlauts('tæst'));
    }

    public function testCheckIBAN(): void
    {
        $this->assertFalse(Text::checkIBAN(''));
        $this->assertFalse(Text::checkIBAN('12345'));
        $this->assertTrue(Text::checkIBAN('DE89370400440532013000'));
        $this->assertSame('DE89370400440532013000', Text::checkIBAN('DE00370400440532013000'));
    }

    public function testCheckBIC(): void
    {
        $this->assertFalse(Text::checkBIC(''));
        $this->assertFalse(Text::checkBIC('123'));
        $this->assertTrue(Text::checkBIC('DEUTDEFF'));
        $this->assertTrue(Text::checkBIC('ABNACHZ8ZRH'));
        $this->assertFalse(Text::checkBIC('123ABNACHZ8ZRH'));
    }
}
