<?php

declare(strict_types=1);

namespace Tests\Helpers;

use JTL\Helpers\Request;
use PHPUnit\Framework\TestCase;

/**
 * Class RequestTest
 * @package Tests\Helpers
 */
final class RequestTest extends TestCase
{
    public function testGetVar(): void
    {
        $_GET['foo'] = 1;
        $this->assertSame(1, Request::getVar('foo'));
        $_GET['foo'] = 'bar';
        $this->assertSame('bar', Request::getVar('foo'));
        unset($_GET['foo']);
        $this->assertSame(42, Request::getVar('foo', 42));
        $this->assertNull(Request::getVar('foo'));
    }

    public function testPostVar(): void
    {
        $_POST['foo'] = 1;
        $this->assertSame(1, Request::postVar('foo'));
        $_POST['foo'] = 'bar';
        $this->assertSame('bar', Request::postVar('foo'));
        unset($_POST['foo']);
        $this->assertSame(42, Request::postVar('foo', 42));
        $this->assertNull(Request::postVar('foo'));
    }

    public function testGetInt(): void
    {
        $_GET['foo'] = 1;
        $this->assertSame(1, Request::getInt('foo'));
        $_GET['foo'] = '42';
        $this->assertSame(42, Request::getInt('foo'));
        unset($_GET['foo']);
        $this->assertSame(42, Request::getInt('foo', 42));
        $this->assertSame(0, Request::getInt('foo'));
    }

    public function testPostInt(): void
    {
        $_POST['foo'] = 1;
        $this->assertSame(1, Request::postInt('foo'));
        $_POST['foo'] = '42';
        $this->assertSame(42, Request::postInt('foo'));
        unset($_POST['foo']);
        $this->assertSame(42, Request::postInt('foo', 42));
        $this->assertSame(0, Request::postInt('foo'));
    }

    public function testHasGPCData(): void
    {
        $_POST['foo'] = 1;
        $this->assertTrue(Request::hasGPCData('foo'));
        unset($_POST['foo'], $_GET['foo']);
        $this->assertFalse(Request::hasGPCData('foo'));
        $_GET['foo'] = 2;
        $this->assertTrue(Request::hasGPCData('foo'));
        $_GET['foo'] = null;
        $this->assertFalse(Request::hasGPCData('foo'));
        unset($_GET['foo']);
    }

    public function testVerifyGPDataIntegerArray(): void
    {
        $_REQUEST['foo'] = [1, 2, '33'];
        $this->assertSame([1, 2, 33], Request::verifyGPDataIntegerArray('foo'));
        $_REQUEST['foo'] = ['foo' => 1, 'bar' => '42'];
        $this->assertSame(['foo' => 1, 'bar' => 42], Request::verifyGPDataIntegerArray('foo'));
        $_REQUEST['foo'] = '33';
        $this->assertSame([33], Request::verifyGPDataIntegerArray('foo'));
        $_REQUEST['foo'] = '';
        $this->assertSame([], Request::verifyGPDataIntegerArray('foo'));
        $_REQUEST['foo'] = [];
        $this->assertSame([], Request::verifyGPDataIntegerArray('foo'));
        unset($_REQUEST['foo']);
        $this->assertSame([], Request::verifyGPDataIntegerArray('foo'));
    }

    public function testVerifyGPCDataInt(): void
    {
        $_POST['foo'] = 1;
        $this->assertSame(1, Request::verifyGPCDataInt('foo'));
        unset($_POST['foo'], $_GET['foo']);
        $this->assertSame(0, Request::verifyGPCDataInt('foo'));
        $_GET['foo'] = 2;
        $this->assertSame(2, Request::verifyGPCDataInt('foo'));
        $_GET['foo'] = null;
        $this->assertSame(0, Request::verifyGPCDataInt('foo'));
        unset($_GET['foo'], $_POST['foo']);
    }

    public function testVerifyGPDataString(): void
    {
        $_POST['foo'] = 'bar';
        $this->assertSame('bar', Request::verifyGPDataString('foo'));
        unset($_POST['foo'], $_GET['foo']);
        $this->assertSame('', Request::verifyGPDataString('foo'));
        $_GET['foo'] = 2;
        $this->assertSame('2', Request::verifyGPDataString('foo'));
        $_GET['foo'] = null;
        $this->assertSame('', Request::verifyGPDataString('foo'));
        unset($_GET['foo'], $_POST['foo']);
    }

    public function testGetRealIP(): void
    {
        // @todo
        $this->assertSame(true, true);
    }

    public function testMakeHTTPHeader(): void
    {
        // @todo
        $this->assertSame(true, true);
    }

    public function testCheckSSL(): void
    {
        // @todo
        $this->assertSame(true, true);
    }

    public function testIsAjaxRequest(): void
    {
        $this->assertFalse(Request::isAjaxRequest());
        $_REQUEST['isAjax'] = true;
        $this->assertTrue(Request::isAjaxRequest());
        unset($_REQUEST['isAjax']);
    }

    public function testExtractExternalParams(): void
    {
        // @todo
        $this->assertSame(true, true);
    }

    public function testUrlHasEqualRequestParameter(): void
    {
        $_GET['foo'] = 'bar';
        $this->assertTrue(Request::urlHasEqualRequestParameter('http://example.com/?foo=bar', 'foo'));
        $this->assertTrue(Request::urlHasEqualRequestParameter('http://example.com/?foo=bar', 'baz'));
        $this->assertFalse(Request::urlHasEqualRequestParameter('http://example.com/?foo=baz', 'foo'));
        unset($_GET['foo']);
    }

    public function testGInt(): void
    {
        $_GET['foo'] = 1;
        $this->assertEquals(1, Request::gInt('foo'));
        $_GET['foo'] = '42';
        $this->assertEquals(42, Request::gInt('foo'));
        $_GET['foo'] = 'test';
        $this->assertEquals(0, Request::gInt('foo'));
        $this->assertEquals(0, Request::gInt('doesnotexist'));
    }

    public function testPInt(): void
    {
        $_POST['foo'] = 1;
        $this->assertEquals(1, Request::pInt('foo'));
        $_POST['foo'] = '42';
        $this->assertEquals(42, Request::pInt('foo'));
        $_POST['foo'] = 'test';
        $this->assertEquals(0, Request::gInt('foo'));
        $this->assertEquals(0, Request::pInt('doesnotexist'));
    }

    public function testGString(): void
    {
        $_GET['foo'] = '1';
        $this->assertEquals('1', Request::gString('foo'));
        $_GET['foo'] = 'test';
        $this->assertEquals('test', Request::gString('foo'));
        $this->assertEquals('', Request::gString('doesnotexist'));
    }

    public function testPString(): void
    {
        $_POST['foo'] = '1';
        $this->assertEquals('1', Request::pString('foo'));
        $_POST['foo'] = 'test';
        $this->assertEquals('test', Request::pString('foo'));
        $this->assertEquals('', Request::pString('doesnotexist'));
    }

    public function testPIntArray(): void
    {
        $_POST['foo'] = ['1', '2', '3', 4, 5];
        $this->assertEquals([1, 2, 3, 4, 5], Request::pIntArray('foo'));
        $this->assertEquals([], Request::pIntArray('indexdoesnotexist'));
        $_POST['foo'] = 'teststring';
        $this->assertEquals([0], Request::pIntArray('foo'));
    }

    public function testGIntArray(): void
    {
        $_GET['foo'] = ['1', '2', '3', 4, 5];
        $this->assertEquals([1, 2, 3, 4, 5], Request::gIntArray('foo'));
        $this->assertEquals([], Request::gIntArray('indexdoesnotexist'));
        $_GET['foo'] = 'teststring';
        $this->assertEquals([0], Request::gIntArray('foo'));
    }
}
