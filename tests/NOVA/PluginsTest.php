<?php

declare(strict_types=1);

namespace Tests\NOVA;

use JTL\Cache\JTLCacheInterface;
use JTL\DB\NiceDB;
use PHPUnit\Framework\Attributes\Depends;
use Template\NOVA\Plugins;
use Tests\UnitTestCase;

class PluginsTest extends UnitTestCase
{
    public function testInstanceCreation(): Plugins
    {
        $db          = $this->createMock(NiceDB::class);
        $cache       = $this->createMock(JTLCacheInterface::class);
        $novaPlugins = new Plugins($db, $cache);
        $this->assertInstanceOf(Plugins::class, $novaPlugins);

        return $novaPlugins;
    }

    #[depends('testInstanceCreation')]
    public function testGetDecimalLength(Plugins $plugins): void
    {
        $this->assertEquals(2, $plugins->getDecimalLength(['quantity' => '12,34']));
        $this->assertEquals(5, $plugins->getDecimalLength(['quantity' => '12.34567']));
        $this->assertEquals(0, $plugins->getDecimalLength(['quantity' => '12']));
        $this->assertEquals(4, $plugins->getDecimalLength(['quantity' => '12.345,5678']));
        $this->assertEquals(2, $plugins->getDecimalLength(['quantity' => 12.34]));
        $this->assertEquals(0, $plugins->getDecimalLength(['quantity' => 'abc,de']));
        $this->assertEquals(0, $plugins->getDecimalLength(['quantity' => 'abc,12']));
        $this->assertEquals(0, $plugins->getDecimalLength(['quantity' => 'abc']));
        $this->assertEquals(0, $plugins->getDecimalLength(['quantity' => null]));
        $this->assertEquals(0, $plugins->getDecimalLength([]));
    }
}
