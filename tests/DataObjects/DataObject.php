<?php

declare(strict_types=1);

namespace Tests\DataObjects;

use JTL\Catalog\Product\Artikel;
use JTL\DataObjects\AbstractDataObject;
use JTL\DataObjects\DataTableObjectInterface;

class DataObject extends AbstractDataObject implements DataTableObjectInterface
{
    private string $primaryKey = 'kTest';

    protected string $name = '';

    protected int $age = 0;

    protected bool $cat = false;

    private ?Artikel $product = null;

    /**
     * @var array<string, string>
     */
    private array $mapping = [
        'kTest'   => 'id',
        'id'      => 'id',
        'vorname' => 'name',
        'name'    => 'name',
        'alter'   => 'age',
        'age'     => 'age',
        'katze'   => 'cat',
        'cat'     => 'cat',
        'artikel' => 'product',
        'product' => 'product',
    ];

    /**
     * @var array<string, string>
     */
    private array $columnMapping = [
        'id'      => 'kTest',
        'name'    => 'cVorname',
        'age'     => 'nAlter',
        'cat'     => 'cKatze',
        'product' => 'cArtikel'
    ];

    /**
     * @inheritdoc
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    /**
     * @inheritdoc
     */
    public function getReverseMapping(): array
    {
        return \array_flip($this->mapping);
    }

    /**
     * @inheritdoc
     */
    public function getColumnMapping(): array
    {
        return $this->columnMapping;
    }

    /**
     * @inheritdoc
     */
    public function getID(): mixed
    {
        return $this->{$this->getPrimaryKey()};
    }

    public function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }

    public function setPrimaryKey(string $primaryKey): DataObject
    {
        $this->primaryKey = $primaryKey;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): DataObject
    {
        $this->name = $name;

        return $this;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function setAge(int $age): DataObject
    {
        $this->age = $age;

        return $this;
    }

    public function hasCat(): bool
    {
        return $this->cat;
    }

    public function getCat(): bool
    {
        return $this->cat;
    }

    public function setCat(bool|int|string $cat): DataObject
    {
        $this->cat = $this->checkAndReturnBoolValue($cat);

        return $this;
    }

    public function getProduct(): Artikel
    {
        return $this->product;
    }

    public function setProduct(Artikel $product): DataObject
    {
        $this->product = $product;

        return $this;
    }
}
