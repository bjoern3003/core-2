<?php

declare(strict_types=1);

namespace Tests\RMA;

use JTL\RMA\DomainObjects\RMADomainObject;
use JTL\RMA\DomainObjects\RMAItemDomainObject;
use JTL\RMA\Helper\RMAItems;
use JTL\RMA\Repositories\RMAItemRepository;
use JTL\RMA\Repositories\RMARepository;
use JTL\RMA\Services\RMAReturnAddressService;
use JTL\RMA\Services\RMAService;
use Tests\UnitTestCase;

class RMAServiceTest extends UnitTestCase
{
    private RMAService $rmaService;
    private RMARepository $RMARepository;
    private RMAItemRepository $RMAItemRepository;
    private RMAReturnAddressService $RMAReturnAddressService;

    /**
     * This method is called before each test.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->RMARepository           = $this->createMock(RMARepository::class);
        $this->RMAItemRepository       = $this->createMock(RMAItemRepository::class);
        $this->RMAReturnAddressService = $this->createMock(RMAReturnAddressService::class);
        $this->rmaService              = new RMAService(
            RMARepository: $this->RMARepository,
            RMAItemRepository: $this->RMAItemRepository,
            RMAReturnAddressService: $this->RMAReturnAddressService,
        );
    }

    /**
     * @throws \Exception
     */
    private function setSomeReturns(): void
    {
        $this->rmaService->rmas = [
            new RMADomainObject(
                items: new RMAItems(items: [
                    new RMAItemDomainObject(
                        shippingNotePosID: 999,
                    ),
                    new RMAItemDomainObject()
                ])
            ),
            new RMADomainObject(
                items: new RMAItems(items: [
                    new RMAItemDomainObject(),
                    new RMAItemDomainObject(),
                    new RMAItemDomainObject()
                ])
            )
        ];
    }

    /**
     * @throws \Exception
     * @comment This test ensures that if some logic is added to the loadReturns() method, it will be tested.
     */
    public function testLoadReturns(): void
    {
        $rmaRepository = $this->RMARepository;
        $rmaRepository->method('getReturns')->willReturn([
            new RMADomainObject(id: 1),
            new RMADomainObject(id: 2)
        ]);

        $this->rmaService->rmas = [];
        $this->rmaService->loadReturns(langID: 0);
        $this->assertCount(
            expectedCount: 2,
            haystack: $this->rmaService->rmas
        );
    }

    /**
     * @throws \Exception
     */
    public function testGetReturn(): void
    {
        $this->RMARepository->method('getReturns')->willReturn(value: [4 => new RMADomainObject(id: 4)]);
        $this->rmaService->rmas = [
            1 => new RMADomainObject(id: 1)
        ];

        $this->assertEquals(
            expected: 1,
            actual: $this->rmaService->getReturn(id: 1)->id
        );
        $this->assertEquals(
            expected: 0,
            actual: $this->rmaService->getReturn(id: 0)->id
        );
        $this->assertEquals(
            expected: 4,
            actual: $this->rmaService->getReturn(id: 4, customerID: 1, langID: 1)->id
        );
    }

    /**
     * @throws \Exception
     */
    public function testNewReturn(): void
    {
        $this->assertEquals(
            expected: 9,
            actual: $this->rmaService->newReturn(customerID: 9)->customerID
        );
    }

    /**
     * @throws \Exception
     */
    public function testGetRMAItems(): void
    {
        $this->setSomeReturns();
        $allRMAItems = [];
        foreach ($this->rmaService->getRMAItems() as $rmaItems) {
            foreach ($rmaItems->getArray() as $item) {
                $allRMAItems[] = $item;
            }
        }

        $this->assertCount(
            expectedCount: 5,
            haystack: $allRMAItems
        );
        $this->assertContainsOnlyInstancesOf(
            className: RMAItemDomainObject::class,
            haystack: $allRMAItems
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testGetOrderArray(): void
    {
        $now      = \date(format: 'd.m.Y');
        $rmaItems = $this->rmaService->getOrderArray(
            rmaItems: new RMAItems(items: [
                new RMAItemDomainObject(orderID: 1, orderNo: 'OrNo1', orderDate: $now),
                new RMAItemDomainObject(orderID: 2, orderNo: 'OrNo2', orderDate: $now),
                new RMAItemDomainObject(orderID: 2, orderNo: 'OrNo3', orderDate: $now)
            ])
        );

        $this->assertSame(
            expected: ['orderNo' => 'OrNo1', 'orderDate' => $now],
            actual: $rmaItems[1]
        );
        $this->assertSame(
            expected: ['orderNo' => 'OrNo2', 'orderDate' => $now],
            actual: $rmaItems[2]
        );
        $this->assertNotTrue(isset($rmaItems[3]));
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testGetOrderNumbers(): void
    {
        $rmaRepository = $this->createStub(originalClassName: RMARepository::class);
        $rmaRepository->method('getOrderNumbers')->willReturn(value: [
            1 => 'OrNu123',
            2 => 'OrNu456'
        ]);
        $localRMAService = new RMAService(
            RMARepository: $rmaRepository,
            RMAItemRepository: $this->RMAItemRepository,
            RMAReturnAddressService: $this->RMAReturnAddressService
        );

        $this->assertSame(
            expected: $this->rmaService->getOrderNumbers(orderIDs: []),
            actual: []
        );

        $this->assertEquals(
            expected: [1 => 'OrNu123', 2 => 'OrNu456'],
            actual: $localRMAService->getOrderNumbers(orderIDs: [1, 2])
        );
    }

    /**
     * @throws \Exception
     */
    public function testGroupRMAItems(): void
    {
        $date1    = \date(format: 'Y-m-d H:i:s');
        $date2    = \date(format: 'Y-m-d H:i:s', timestamp: \strtotime(datetime: '-1 day'));
        $rmaItems = new RMAItems(items: [
            new RMAItemDomainObject(productID: 3, reasonID: 5, status: 'a', createDate: $date1, orderNo: '1'),
            new RMAItemDomainObject(productID: 4, reasonID: 6, status: 'b', createDate: $date2, orderNo: '2'),
            new RMAItemDomainObject(productID: 4, reasonID: 6, status: 'b', createDate: $date2, orderNo: '2')
        ]);

        $groupedOrder   = $this->rmaService->groupRMAItems(rmaItems: $rmaItems);
        $groupedProduct = $this->rmaService->groupRMAItems(rmaItems: $rmaItems, by: 'product');
        $groupedReason  = $this->rmaService->groupRMAItems(rmaItems: $rmaItems, by: 'reason');
        $groupedStatus  = $this->rmaService->groupRMAItems(rmaItems: $rmaItems, by: 'status');
        $groupedDate    = $this->rmaService->groupRMAItems(rmaItems: $rmaItems, by: 'day');


        $this->assertTrue(
            condition: isset($groupedOrder[1], $groupedOrder[2]) && \count($groupedOrder[2]) === 2
        );
        $this->assertTrue(
            condition: isset($groupedProduct[3], $groupedProduct[4]) && \count($groupedProduct[4]) === 2
        );
        $this->assertTrue(
            condition: isset($groupedReason[5], $groupedReason[6]) && \count($groupedReason[6]) === 2
        );
        $this->assertTrue(
            condition: isset($groupedStatus['a'], $groupedStatus['b']) && \count($groupedStatus['b']) === 2
        );
        $this->assertTrue(
            condition: isset(
                $groupedDate[\date(format: 'd.m.Y', timestamp: \strtotime(datetime: $date1))],
                $groupedDate[\date(format: 'd.m.Y', timestamp: \strtotime(datetime: $date2))]
            ) && count($groupedDate[\date(format: 'd.m.Y', timestamp: \strtotime(datetime: $date2))]) === 2
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testGetReturnableProducts(): void
    {
        $this->RMARepository->method('getReturnableProducts')->willReturn(value: [
            new RMAItemDomainObject(id: 1, rmaID: 1),
            new RMAItemDomainObject(id: 2, rmaID: 1),
            new RMAItemDomainObject(id: 3, rmaID: 2)
        ]);

        $this->assertCount(
            expectedCount: 3,
            haystack: $this->rmaService->getReturnableProducts()
        );
    }

    /**
     * @throws \Exception
     */
    public function testGetRMAItem(): void
    {
        $this->setSomeReturns();

        // This one does exist
        $this->assertSame(
            expected: 999,
            actual: $this->rmaService->getRMAItem(
                rma: $this->rmaService->rmas[0],
                shippingNotePosID: 999
            )->shippingNotePosID
        );

        // This one does not exist
        $this->assertNotSame(
            expected: 123,
            actual: $this->rmaService->getRMAItem(
                rma: $this->rmaService->rmas[1],
                shippingNotePosID: 123
            )->shippingNotePosID
        );
    }

    /**
     * @return void
     */
    public function testHashCreateDate(): void
    {
        $this->setSomeReturns();

        $this->assertSame(
            expected: \strlen(string: $this->rmaService->hashCreateDate(rma: $this->rmaService->rmas[0])),
            actual: 32
        );
    }
}
