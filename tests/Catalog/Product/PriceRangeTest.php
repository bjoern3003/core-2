<?php

declare(strict_types=1);

namespace Tests\Catalog\Product;

use JTL\Catalog\Product\PriceRange;
use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use PHPUnit\Framework\MockObject\Exception;
use Tests\UnitTestCase;

class PriceRangeTest extends UnitTestCase
{
    private DbInterface $db;

    private const TAX = 19.0;

    private float $tax = (1 + self::TAX / 100);

    private float $min = 100.0;

    private float $max = 110.0;

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->db = $this->createMock(NiceDB::class);
    }

    private function getInstance(): MockPriceRange
    {
        $_SESSION['Steuersatz'][0] = self::TAX;

        $priceRange = new MockPriceRange(1, 1, 1, $this->db);
        $this->assertInstanceOf(PriceRange::class, $priceRange);

        return $priceRange;
    }

    private function testDiscount(PriceRange $pr, float $discount): void
    {
        $min = $this->min * (1 - $discount / 100);
        $max = $this->max * (1 - $discount / 100);
        $pr->setDiscount($discount);
        $this->assertEquals($min, $pr->minNettoPrice);
        $this->assertEquals(\round($min * $this->tax, 2), $pr->minBruttoPrice);
        $this->assertEquals($max, $pr->maxNettoPrice);
        $this->assertEquals(\round($max * $this->tax, 2), $pr->maxBruttoPrice);
    }

    private function testNoDiscount(PriceRange $pr): void
    {
        $pr->setDiscount(10);
        $this->assertEquals($this->min, $pr->minNettoPrice);
        $this->assertEquals(\round($this->min * $this->tax, 2), $pr->minBruttoPrice);
        $this->assertEquals($this->max, $pr->maxNettoPrice);
        $this->assertEquals(\round($this->max * $this->tax, 2), $pr->maxBruttoPrice);
    }

    private function testFullDiscount(PriceRange $pr): void
    {
        $pr->setDiscount(100);
        $this->assertEquals(0.0, $pr->minNettoPrice);
        $this->assertEquals(0.0, $pr->minBruttoPrice);
        $this->assertEquals(0.0, $pr->maxNettoPrice);
        $this->assertEquals(0.0, $pr->maxBruttoPrice);
    }

    public function testSetDiscount(): void
    {
        $pr = $this->getInstance();

        $pr->minNettoPrice = $this->min;
        $pr->maxNettoPrice = $this->max;
        $pr->mockData($this->min, $this->max, $this->tax);
        $this->testDiscount($pr, 10.0);
        $this->testDiscount($pr, 20.0);
        $this->testDiscount($pr, 10.0);
        $this->testDiscount($pr, 0.0);

        $pr->isMinSpecialPrice = true;
        $pr->isMaxSpecialPrice = true;
        $this->testNoDiscount($pr);

        $pr->isMinSpecialPrice = false;
        $pr->isMaxSpecialPrice = false;
        $this->testFullDiscount($pr);
        $this->testDiscount($pr, 10);
    }
}
