<?php

declare(strict_types=1);

namespace Tests\Catalog\Product;

use JTL\Catalog\Product\PriceRange;

/**
 * Class MockPriceRange
 * @package Tests\Catalog\Product
 */
class MockPriceRange extends PriceRange
{
    private float $fVKNettoMin = 0;

    private float $fVKNettoMax = 0;

    private float $tax = 19.0;

    public function mockData(float $fVKNettoMin, float $fVKNettoMax, float $tax): self
    {
        $this->fVKNettoMin = $fVKNettoMin;
        $this->fVKNettoMax = $fVKNettoMax;
        $this->tax         = $tax;

        return $this;
    }

    protected function loadPriceRange(): void
    {
        $this->minNettoPrice  = $this->fVKNettoMin;
        $this->maxNettoPrice  = $this->fVKNettoMax;
        $this->minBruttoPrice = \round($this->minNettoPrice * (1 + $this->tax), 4);
        $this->maxBruttoPrice = \round($this->maxNettoPrice * (1 + $this->tax), 4);
    }
}
