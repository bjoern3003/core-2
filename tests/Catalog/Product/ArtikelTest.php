<?php

declare(strict_types=1);

namespace Tests\Catalog\Product;

use JTL\Catalog\Product\Artikel;
use Tests\UnitTestCase;

class ArtikelTest extends UnitTestCase
{
    private function createArtikel(string $cName, int $nSort): Artikel
    {
        $product        = $this->getMockBuilder(className: Artikel::class)
            ->disableOriginalConstructor()
            ->getMock();
        $product->cName = $cName;
        $product->nSort = $nSort;

        return $product;
    }

    public function testSortVarCombinationArray(): void
    {
        $products = [
            $this->createArtikel('Test3', 9),
            $this->createArtikel('Test8', 3),
            $this->createArtikel('Test2', 7),
            $this->createArtikel('Test3', 2),
            $this->createArtikel('Test4', 1),
            $this->createArtikel('Test4', 4),
            $this->createArtikel('Test1', 9),
        ];
        $results  = [
            [1, 'Test4'],
            [2, 'Test3'],
            [3, 'Test8'],
            [4, 'Test4'],
            [7, 'Test2'],
            [9, 'Test1'],
            [9, 'Test3'],
        ];

        $product = new Artikel();
        $product->sortVarCombinationArray($products);
        $pos = 0;
        foreach ($products as $item) {
            $this->assertEquals($results[$pos][0], $item->nSort);
            $this->assertEquals($results[$pos][1], $item->cName);
            $pos++;
        }
    }
}
