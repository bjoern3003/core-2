# Struktur

## Allgemeines zum Template-System von JTL-Shop

JTL-Shop nutzt das Template-System [Smarty](https://www.smarty.net/), welches die serverseitige Anwendungslogik von
der Präsentation (dem Template) trennt. Im NOVA-Template setzen wir anstelle von CSS/LESS das modernere SCSS ein um
die Stylebeschreibungen strukturiert und anpassbar zu halten.

Das Nova-Template beinhaltet 3 verschiedene Design-Themen (im Folgenden "*Themes*" genannt), welche die Darstellung
des Shops beeinflussen. Diese sind "*blackline*", "*midnight*" und "*clear*", unsere Standard-Theme. Im
Admin-Backend unter "*[Templates|Template-Einstellungen] -> [Template-Name] -> Einstellungen*" bestimmt der
Shop-Betreiber ein Standard-Theme, welches im Shop aktiv ist.

!!! note
    Alle Erläuterungen und Anleitungen der folgenden Seiten beziehen sich auf das JTL-Standard-Template. 

## Ordnerstruktur

Alle JTL-Shop-Templates befinden sich im Ordner `<Shop-Root>/templates/`.

    templates/Nova/
    ├── account/
    ├── basket/
    ├── blog/
    ├── boxes/
    ├── checkout/
    ├── comparelist/
    ├── contact/
    ├── images/
    ├── js/
    ├── layout/
    ├── locale/
    ├── newsletter/
    ├── page/
    ├── php/
    ├── productdetails/
    ├── productlist/
    ├── register/
    ├── selectionwizard/
    ├── snippets/
    ├── themes/
    ├── Bootstrap.php
    ├── Plugins.php
    ├── preview.png
    └── template.xml


In jedem Template-Ordner muss zwingend eine `template.xml` vorhanden sein.

### Ordner: themes

Ein Theme im JTL-Shop-Template definiert per CSS das Design des Shop-Templates. Im Nova-Template wurden die
Styleregeln in SCSS geschrieben und anschließend zu CSS kompiliert. Das hat den Vorteil, dass der Code besser
les- und wart-bar ist.

Themes liegen in Unterordnern im Verzeichnis `templates/[Template-Name]/themes/`. Weitere Informationen zur
Theme-Struktur und Theme-Anpassung finden Sie unter "[eigenes_theme](eigenes_theme.md)".

## Die `template.xml`

Jedes Template verfügt über eine Datei namens `template.xml`, welche die grundlegenden Einstellungen des jeweiligen
Templates beinhaltet. Diese Einstellungen werden vom Shop automatisch eingelesen und im Admin-Backend unter
"*Templates|Template-Einstellungen -> [Template-Name] -> Einstellungen*" aufgelistet.

In der `template.xml` werden neben den verfügbaren Template-Einstellungen auch Basisinformationen in Form von Tags
angegeben.

Diese Tags sollten immer gefüllt sein:

| Tag | Beschreibung |
| - | - |
| `<Name>` | Name des Templates (wird unter "*Templates*" als Name des Templates angezeigt) |
| `<Author>` | Name des Autors (wird unter "*Templates*" als Name des Autors angezeigt) |
| `<URL>` | Autor-URL (wird unter "*Templates*" als Verlinkung zum Autor angezeigt) |
| `<Version>` | Template-Version |
| `<MinShopVersion>` | Shop-Version |
| `<Preview>` | Pfad zum Vorschaubild ausgehend vom aktuellen Verzeichnis des Templates |
| `<Descrption>` | Kurzbeschreibung des Templates (wird unterhalb des Template-Namens unter "*Templates*" angezeigt) |

Um die Einstellungen besser lesbar zu machen und für den Shopbetreiber zu gruppieren, können sie das Attribut
"MarginBottom" nutzen. Hier ein Beispiel:

    <Section Name="Colors" Key="colors">
            <Setting Description="colorPrimary" Key="primary" Type="colorpicker" Value=""/>
            <Setting Description="colorSecondary" Key="secondary" Type="colorpicker" Value="" MarginBottom="true"/>
            ...
    </Section>

Es besteht zudem die Möglichkeit eine Überschrift über der aktuellen Einstellung einzufügen. Im folgenden Code
erweitern wir das vorangegangene Beispiel:

    <Section Name="Colors" Key="colors">
            <Setting Description="colorPrimary" Key="primary" Type="colorpicker" Value=""/>
            <Setting Description="colorSecondary" Key="secondary" Type="colorpicker" Value="" MarginBottom="true"/>
            <Setting Description="colorHeaderBgColor" Key="header-bg-color" Type="colorpicker" Value="" Subheader="header header"/>
    </Section>

Neben template-spezifischen Einstellungen werden in der `template.xml` auch die verfügbaren Themes und die zu
inkludierenden CSS/JS-Dateien definiert.
Eine Anleitung zum Erstellen eines eigenen Themes finden Sie im Abschnitt "[eigenes_theme](eigenes_theme.md)".