# HOOK_BOXEN_INC_SUCHWOLKE (87)

## Triggerpunkt

Vor der Anzeige der "Suchwolke"

## Parameter

* `JTL\Boxes\Items\SearchCloud` **&box** - "Suchwolke"-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"