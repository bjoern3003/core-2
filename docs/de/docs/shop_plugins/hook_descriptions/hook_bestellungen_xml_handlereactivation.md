# HOOK_BESTELLUNGEN_XML_HANDLEREACTIVATION

## Triggerpunkt

In `JTL\dbeS\Sync\Orders::handleReactivation` vor der Reaktivierung einer Bestellung beim Abgleich mit JTL-Wawi.

## Parameter

* `int` **orderId** - die ID der Bestellung
* `bool|JTL\Plugin\Payment\LegacyMethod` **module** - die Instanz der Zahlungsart, wenn es sich um eine Plugin-Zahlungsart handelt, ansonsten ``false``