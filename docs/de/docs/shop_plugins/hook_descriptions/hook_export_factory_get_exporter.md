# HOOK_EXPORT_FACTORY_GET_EXPORTER (352)

## Triggerpunkt

Nach dem Laden eines Export-Models und vor dessen Initialisierung

## Parameter

* `JTL\Export\FormatExporter` **exporter** - Instanz von FormatExporter
* `int` **exportID** - ID des geladenen Exportformats
* `JTL\Export\Model` **model** - Das zum Exportformat gehörige DataModel