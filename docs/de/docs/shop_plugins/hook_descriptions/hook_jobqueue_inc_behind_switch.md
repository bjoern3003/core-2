# HOOK_JOBQUEUE_INC_BEHIND_SWITCH (158)

## Triggerpunkt

Nach dem Ausführen eines Cron-Jobs

## Parameter

* `JTL\Cron\QueueEntry` **oJobQueue** - Cron-Job-Warteschlange
* `JTL\Cron\JobInterface` **job** - (ab Version 5.0) CronJob