#!/usr/bin/env sh

export REPOSITORY_DIR=$1
XUID=$2
XGID=$3

echo "] execute 'composer install'.."
composer install -o -q -d "${REPOSITORY_DIR}"/includes

echo "] get security checker.."
wget https://github.com/fabpot/local-php-security-checker/releases/download/v1.0.0/local-php-security-checker_1.0.0_linux_amd64;
mv local-php-security-checker_1.0.0_linux_amd64 local-php-security-checker && chmod 775 local-php-security-checker;

echo "] prepare all sources for re-use.."
chown -R "${XUID}"."${XGID}" .

echo "] check composer packages vulnerabilities..";
result=$(./local-php-security-checker --path="${REPOSITORY_DIR}/includes/composer.lock")
errorcode=$?
if [ $errorcode -ne 0 ]; then
    printf "\e[1;31m Error: %s\e[0m \n" "${result}"
    exit 1;
else
    echo "$result"
fi

echo "] execute tests..";
result=$("${REPOSITORY_DIR}"/includes/vendor/bin/phpunit --do-not-cache-result --bootstrap tests/bootstrap.php tests)
errorcode=$?
if [ $errorcode -ne 0 ]; then
    printf "\e[1;31m unit tests failed: %s\e[0m \n" "${result}"
    exit 1;
else
    echo "] unit tests finished."
fi
